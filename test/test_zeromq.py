'''
Created on Aug 10, 2013

@author: ehsan
'''

import unittest
from playlistmaker.mq.zero_mq import ZeroMQ


class TestSnakeMQ(unittest.TestCase):

    def setUp(self):
        self.mq = ZeroMQ('127.0.0.1', 5559, 5560)

    def tearDown(self):
        self.mq.tearDown()

    def testSendReceive1(self):
        print 'test-1'
        self.mq.getEndPoint1().write('message1')
        msg = self.mq.getEndPoint2().read(timeout=1)
        self.assertTrue(msg is None or msg == 'message1')
     
    def testSendReceive2(self):
        print 'test-2'
        self.mq.getEndPoint2().write('message1')
        msg = self.mq.getEndPoint1().read(timeout=1)
        self.assertTrue(msg is None or msg == 'message1')
    
    def testSendReceive3(self):
        print 'test-3'
        self.mq.getEndPoint1().write('message1')        
        msg = self.mq.getEndPoint2().read(timeout=1)        
        self.assertTrue(msg is None or msg == 'message1')
        self.mq.getEndPoint2().write('message2')
        msg = self.mq.getEndPoint1().read(timeout=1)        
        self.assertTrue(msg is None or msg == 'message2')
        self.mq.getEndPoint1().write('message11')        
        msg = self.mq.getEndPoint2().read(timeout=1)
        self.assertTrue(msg is None or msg == 'message11')
        self.mq.getEndPoint2().write('message22')
        self.mq.getEndPoint2().write('message222')
        msg = self.mq.getEndPoint1().read(timeout=1)   
        self.assertEqual('message22', msg)
        msg = self.mq.getEndPoint1().read(timeout=1)   
        self.assertEqual('message222', msg)
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()