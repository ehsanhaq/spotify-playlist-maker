'''
Created on Aug 16, 2013

@author: ehsan
'''
import unittest

testmodules = [
    'test_json_parser',
    'test_xml_parser',
    'test_leveldb',
    'test_memcached',
    'test_cross_import_export',
    'test_metastore',
    'test_playlist_maker',
    'test_ratelimit',
    'test_text_tokenizer',
    'test_generation_tokenizer',
#     'test_codernitydb',
#     'test_snakemq',
#     'test_zeromq',
    ]

suite = unittest.TestSuite()

for t in testmodules:
    try:
        # If the module defines a suite() function, call it to get the suite.
        mod = __import__(t, globals(), locals(), ['suite'])
        suitefn = getattr(mod, 'suite')
        suite.addTest(suitefn())
    except (ImportError, AttributeError):
        # else, just load all the test cases from the module.
        suite.addTest(unittest.defaultTestLoader.loadTestsFromName(t))

unittest.TextTestRunner().run(suite)
