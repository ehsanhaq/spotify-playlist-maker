'''
Created on Aug 15, 2013

@author: ehsan
'''
import unittest
from playlistmaker.spotifyapi.xml_parser import XMLTrackListParser
import ast

class TestXMLParser(unittest.TestCase):
    
    def testHeaderWithEmptyTrackList(self):
        xml = \
'<?xml version="1.0" encoding="utf-8"?>' + \
'<tracks xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/" xmlns="http://www.spotify.com/ns/music/1">' + \
'  <opensearch:Query role="request" startPage="1" searchTerms="let it go"/>' + \
'  <opensearch:totalResults>7094</opensearch:totalResults>' + \
'  <opensearch:startIndex>0</opensearch:startIndex>' + \
'  <opensearch:itemsPerPage>100</opensearch:itemsPerPage>' + \
'</tracks>'
        namespaces = {'opensearch': 'http://a9.com/-/spec/opensearch/1.1/', \
                      'xmlns':'http://www.spotify.com/ns/music/1'}
        parser = XMLTrackListParser(xml, namespaces)
        expected = {'query': 'let it go', 'total': 7094, 'limit': 100, 'page': 0, 'offset': 0}        
        self.assertEqual(expected, parser.parsed['header'])
        self.assertEqual({}, parser.parsed['tracks'])
    
    def testHeaderWithDuplicateTracks(self):
        xml = \
'<?xml version="1.0" encoding="utf-8"?>' + \
'<tracks xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/" xmlns="http://www.spotify.com/ns/music/1">' + \
'  <opensearch:Query role="request" startPage="1" searchTerms="Song"/>' + \
'  <opensearch:totalResults>3</opensearch:totalResults>' + \
'  <opensearch:startIndex>0</opensearch:startIndex>' + \
'  <opensearch:itemsPerPage>100</opensearch:itemsPerPage>' + \
'  <track href="href1">' + \
'    <name>Song 1</name>' + \
'    <length>197.498000</length>' + \
'    <popularity>0.62</popularity>' + \
'  </track>' + \
'  <track href="href2">' + \
'    <name>Song 2</name>' + \
'    <length>197.498000</length>' + \
'    <popularity>0.62</popularity>' + \
'  </track>' + \
'  <track href="href3">' + \
'    <name>Song 1</name>' + \
'    <length>98.498000</length>' + \
'    <popularity>0.67</popularity>' + \
'  </track>' + \
'</tracks>'
        namespaces = {'opensearch': 'http://a9.com/-/spec/opensearch/1.1/', \
                      'xmlns':'http://www.spotify.com/ns/music/1'}
        parser = XMLTrackListParser(xml, namespaces)
        expectedHeader = {'query': 'Song', 'total': 3, 'limit': 100, 'page': 0, 'offset': 0}        
        self.assertEqual(expectedHeader, parser.parsed['header'])
        expectedTracks = {'song 1': {'length': 98.498, 'href': 'href3', 'name': 'Song 1', 'popularity': 0.67}, \
                          'song 2': {'length': 197.498, 'href': 'href2', 'name': 'Song 2', 'popularity': 0.62}}        
        self.assertEqual(expectedTracks, parser.parsed['tracks'])
    
    def testWithActualDataXML(self):
        namespaces = {'opensearch': 'http://a9.com/-/spec/opensearch/1.1/', \
                      'xmlns':'http://www.spotify.com/ns/music/1'}
        parser = XMLTrackListParser(open('data/sample.xml').read(), namespaces)
        expected = ast.literal_eval(open('data/expected_sample_xml.txt').read())        
        self.assertEqual(expected, parser.parsed)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()