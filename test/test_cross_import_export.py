'''
Created on Aug 17, 2013

@author: ehsan
'''
import unittest
from playlistmaker.datastore.memcached_store import MemcacheStore
from playlistmaker.datastore.leveldb_store import LevelDBStore
import tempfile
import os


class TestCrossImportExportDB(unittest.TestCase):

    def testImportFromMemCacheExportToLevelDB(self):
        (_, fName) = tempfile.mkstemp()
        memcacheDB = MemcacheStore(['127.0.0.1'], flush = True)
        memcacheDB.put('key', 'value')
        memcacheDB.put('key1', 'value1')
        memcacheDB.put('key2', 'value2')
        memcacheDB.put('key3', 'value3 with spaces')
        memcacheDB.put('key4', {1:'ehsan', 2:'iname'})                                
        memcacheDB.put('Ivan Krsti\xc4\x87', 'Ivan Krsti\xc4\x87')
        memcacheDB.save(fName)
        levelDB = LevelDBStore(tempfile.mkdtemp())                
        levelDB.load(fName)
        os.remove(fName)
        self.assertEqual('value', levelDB.get('key'))
        self.assertEqual('value1', levelDB.get('key1'))
        self.assertEqual('value2', levelDB.get('key2'))
        self.assertEqual('value3 with spaces', levelDB.get('key3'))
        self.assertEqual({1:'ehsan', 2:'iname'}, levelDB.get('key4'))
        self.assertEqual('Ivan Krsti\xc4\x87', levelDB.get('Ivan Krsti\xc4\x87'))
    
    def testImportFromLevelDBExportToMemCache(self):
        levelDB = LevelDBStore(tempfile.mkdtemp())
        (_, fName) = tempfile.mkstemp()
        levelDB.put('key', 'value')
        levelDB.put('key1', 'value1')
        levelDB.put('key2', 'value2')
        levelDB.put('key3', 'value3 with spaces')
        levelDB.put('key4', {1:'ehsan', 2:'iname'})
        levelDB.put('Ivan Krsti\xc4\x87', 'Ivan Krsti\xc4\x87')
        levelDB.save(fName)
        memcacheDB = MemcacheStore(['127.0.0.1'], flush = True)
        memcacheDB.load(fName)
        os.remove(fName)
        self.assertEqual('value', memcacheDB.get('key'))
        self.assertEqual('value1', memcacheDB.get('key1'))
        self.assertEqual('value2', memcacheDB.get('key2'))
        self.assertEqual('value3 with spaces', memcacheDB.get('key3'))
        self.assertEqual({1:'ehsan', 2:'iname'}, memcacheDB.get('key4'))
        self.assertEqual('Ivan Krsti\xc4\x87', memcacheDB.get('Ivan Krsti\xc4\x87'))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()