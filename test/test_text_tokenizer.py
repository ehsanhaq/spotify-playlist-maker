'''
Created on Jul 27, 2013

@author: ehsan
'''
import unittest
from sets import Set
from playlistmaker.util.tokenizer import TextTokenizer

class TestTextTokenizer(unittest.TestCase):

    def testSimpleTokenizerObjectTest(self):
        tokenizer = TextTokenizer('This is a text')
        self.assertEqual('This', tokenizer.next_token())
        self.assertEqual('is', tokenizer.next_token())
        self.assertEqual('a', tokenizer.next_token())
        self.assertEqual('text', tokenizer.next_token())
        self.assertFalse(tokenizer.has_more_tokens())
    
    def testTokenizerStaticTest(self):
        tokens = TextTokenizer.generate_tokens('This is a text')
        self.assertEqual(['This', 'is', 'a', 'text'], tokens)
        
        tokens = TextTokenizer.generate_tokens('This is a text', diatomics=Set(['is', 'a']))
        self.assertEqual(['This', 'is a', 'text'], tokens)
        
        tokens = TextTokenizer.generate_tokens('This,is,a,text', delimeter=',', diatomics=Set(['is', 'a']))
        self.assertEqual(['This', 'is,a', 'text'], tokens)
    
    def testTokenizerObjectTest(self):
        tokenizer = TextTokenizer('This is a text', delimeter=',')
        self.assertEqual('This is a text', tokenizer.next_token())        
        self.assertFalse(tokenizer.has_more_tokens())
        
        tokenizer = TextTokenizer('This is a text', delimeter=' ', diatomics=Set(['a', 'is']))
        self.assertEqual('This', tokenizer.next_token())
        self.assertTrue(tokenizer.has_more_tokens())
        self.assertEqual('is a', tokenizer.next_token())
        self.assertEqual('text', tokenizer.next_token())
        self.assertFalse(tokenizer.has_more_tokens())
        
        tokenizer = TextTokenizer('This is a text', delimeter=' ', diatomics=Set(['This', 'is', 'This is']))
        self.assertEqual('This is a', tokenizer.next_token())        
        self.assertTrue(tokenizer.has_more_tokens())        
        self.assertEqual('text', tokenizer.next_token())
        self.assertFalse(tokenizer.has_more_tokens())
    
    def testTokenizerObjectTestWithModifyingDiatomics(self):
        tokenizer = TextTokenizer('This is a text')
        tokenizer.add_diatomic('is')
        tokenizer.add_diatomic('a')
        tokenizer.add_diatomic('is a')
        self.assertEqual('This', tokenizer.next_token())
        self.assertEqual('is a text', tokenizer.next_token())        
        self.assertFalse(tokenizer.has_more_tokens())
        
        tokenizer = TextTokenizer('This is a text This is a text', diatomics=Set(['is', 'a']))
        self.assertEqual('This', tokenizer.next_token())
        tokenizer.add_diatomic('This')
        self.assertEqual('is a', tokenizer.next_token())
        tokenizer.add_diatomic('is a')
        self.assertEqual('text', tokenizer.next_token())
        self.assertEqual('This is', tokenizer.next_token())
        self.assertEqual('a text', tokenizer.next_token())
        self.assertFalse(tokenizer.has_more_tokens())
    
    def testCaseInsensitiveCompare(self):
        tokenizer = TextTokenizer('This IS A text This IS A text', diatomics=Set(['is', 'a']))
        self.assertEqual('This', tokenizer.next_token())
        self.assertEqual('IS A', tokenizer.next_token())
        self.assertEqual('text', tokenizer.next_token())
        self.assertEqual('This', tokenizer.next_token())
        self.assertEqual('IS A', tokenizer.next_token())
        self.assertEqual('text', tokenizer.next_token())
    
    def testMinimumWords(self):
        tokenizer = TextTokenizer('This IS A text This IS A text', minWords=2)
        self.assertEqual('This IS', tokenizer.next_token())
        self.assertEqual('A text', tokenizer.next_token())
        self.assertEqual('This IS', tokenizer.next_token())
        self.assertEqual('A text', tokenizer.next_token())
    
    def testMaximumWords(self):
        tokenizer = TextTokenizer('This IS A text This IS A text', minWords=10, maxWords=3)
        self.assertEqual('This IS A', tokenizer.next_token())
        self.assertEqual('text This IS', tokenizer.next_token())
        self.assertEqual('A text', tokenizer.next_token())

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()