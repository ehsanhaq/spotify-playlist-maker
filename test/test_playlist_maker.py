'''
Created on Aug 11, 2013

@author: ehsan
'''
import unittest
from playlistmaker.datastore.leveldb_store import LevelDBStore
import tempfile
from playlistmaker.playlist_maker import TextToPlayList
from playlistmaker.spotifyapi.metastore import SpotifyMetaStoreAPI
import threading
import time
import Queue


class TestPlayListMaker(unittest.TestCase):

    def setUp(self):
        self.db = LevelDBStore(tempfile.mkdtemp())        

    def tearDown(self):
        pass

    def test_addDiatomic_and_getDiatomicFor(self):
        m = TextToPlayList(self.db, None)
        self.assertEqual([], m._getDiatomicFor('token1'))
        m._addDiatomic('token1', 'token1 word1')
        self.assertEqual(['token1 word1'], m._getDiatomicFor('token1'))
        m._addDiatomic('token1', 'token1 word2')
        # assertItemsEqual compares without regarding order.
        self.assertItemsEqual(['token1 word1', 'token1 word2'], m._getDiatomicFor('token1'))
        self.assertItemsEqual(['token1 word2', 'token1 word1'], m._getDiatomicFor('token1'))
        
        m._addDiatomic('token2', 'token2 word1')
        m._addDiatomic('token2', 'token2 word2')
        m._addDiatomic('token2', 'token2 word3')
        self.assertItemsEqual(['token1 word1', 'token1 word2'], m._getDiatomicFor('token1'))
        self.assertItemsEqual(['token2 word1', 'token2 word2', 'token2 word3'], m._getDiatomicFor('token2'))
        
        m._addDiatomic('token3', 'token3')
        self.assertItemsEqual(['token3'], m._getDiatomicFor('token3'))
    
    def test_getDiatomics(self):
        m = TextToPlayList(self.db, None)
        # tokens which will be searched.
        m._addDiatomic('token3', 'token3')
        m._addDiatomic('token2', 'token2 word1')
        m._addDiatomic('token2', 'token2 word2')
        m._addDiatomic('token2', 'token2 word3')
        m._addDiatomic('token1', 'token1 word2')
        m._addDiatomic('token1', 'token1 word1')
        # tokens that will not be searched.
        m._addDiatomic('token4', 'token4 word1')
        m._addDiatomic('token4', 'token4 word2')
        m._addDiatomic('token5', 'token5 word1')
        m._addDiatomic('token6', 'token6')
        expected = ['token1 word1', 'token1 word2', \
                    'token2 word1', 'token2 word2', 'token2 word3', \
                    'token3']
        self.assertItemsEqual(expected, m._getDiatmoics('token1 token2 token3', ' '))
    
    def test_filterAndScoreSearchResultsPositive(self):
        r = TextToPlayList._filterAndScoreSearchResults ({'header': {'query': 'song 1'}, 'tracks': {'song 1':{}}})        
        self.assertEqual({'header': {'query': 'song 1'}, 'tracks': \
                          {'song 1': {'score': 1.0}}, 'topmatch': 'song 1'}, r)
        r = TextToPlayList._filterAndScoreSearchResults ({'header': {'query': 'song 1'}, 'tracks': {'song 2':{}}})        
        self.assertEqual({'header': {'query': 'song 1'}, 'tracks': \
                          {'song 2': {'score': 0.83}}, 'topmatch': 'song 2'}, r)
        r = TextToPlayList._filterAndScoreSearchResults ({'header': {'query': 'song 1'}, \
                                                          'tracks': {'song 2':{}, 'song 1': {}}})        
        self.assertEqual({'header': {'query': 'song 1'}, 'tracks': \
                          {'song 1': {'score': 1.0}}, 'topmatch': 'song 1'}, r)
        r = TextToPlayList._filterAndScoreSearchResults ({'header': {'query': 'song 1'}, \
                                                          'tracks': {'song 2':{}, 'song 1': {}}}, 2)        
        self.assertEqual({'header': {'query': 'song 1'}, 'tracks': \
                          {'song 1': {'score': 1.0}, 'song 2': {'score': 0.83}}, 'topmatch': 'song 1'}, r)
        r = TextToPlayList._filterAndScoreSearchResults ({'header': {'query': 'song 1'}, \
                                                          'tracks': {'song 2':{}, 'song 1': {}, 'xxxxx': {}}}, 2)
        self.assertEqual({'header': {'query': 'song 1'}, 'tracks': \
                          {'song 1': {'score': 1.0}, 'song 2': {'score': 0.83}}, 'topmatch': 'song 1'}, r)
        
    def test_filterAndScoreSearchResultsNegative(self):
        self.assertEqual(None, TextToPlayList._filterAndScoreSearchResults({}))
        self.assertEqual(None, TextToPlayList._filterAndScoreSearchResults({'header': {}}))
        self.assertEqual(None, TextToPlayList._filterAndScoreSearchResults({'tracks': {}}))
        self.assertEqual(None, TextToPlayList._filterAndScoreSearchResults({'header': {}, 'tracks': {}}))
        self.assertEqual({'header': {'query': 'song 1'}, 'tracks':{}}, \
                         TextToPlayList._filterAndScoreSearchResults({'header': {'query': 'song 1'}, 'tracks': {}}))
        self.assertRaises(AttributeError, TextToPlayList._filterAndScoreSearchResults, \
                          ({'header': {'query': []}, 'tracks': {}}))
        self.assertRaises(AttributeError, TextToPlayList._filterAndScoreSearchResults, \
                          ({'header': {'query': 'song 1'}, 'tracks': []}))
    
    def test_getWeightedScorePositive(self):
        self.assertEqual(0.0, TextToPlayList._getWeightedScore([]))
        resultList = [{'topmatch':'song 1', 'tracks':{'song 1':{'score': 0.7}, 'song 11':{'score': 0.4}}}]
        self.assertEqual(0.7, TextToPlayList._getWeightedScore(resultList))
        resultList = [{'topmatch':'song 1', 'tracks':{'song 1':{'score': 1.0}, 'song 11':{'score': 0.8}}}, 
                      {'topmatch':'song 2', 'tracks':{'song 2':{'score': 1.0}, 'song 21':{'score': 0.8}}}]
        self.assertEqual(1.0, TextToPlayList._getWeightedScore(resultList))
        resultList = [{'topmatch':'song 1', 'tracks':{'song 1':{'score': 0.0}}}, 
                      {'topmatch':'song 2', 'tracks':{'song 2':{'score': 0.0}}}]
        self.assertEqual(0.0, TextToPlayList._getWeightedScore(resultList))
        resultList = [{'topmatch':'song 1', 'tracks':{'song 1':{'score': 0.9}, 'song 11':{'score': 0.8}}}, 
                      {'topmatch':'song 2', 'tracks':{'song 2':{'score': 0.8}, 'song 21':{'score': 0.7}}}]
        self.assertEqual(0.85, round(TextToPlayList._getWeightedScore(resultList), 2))
    
    def test_getWeightedScoreNegitive(self):        
        resultList = [{'tracks':{'song 1':{'score': 0.7}, 'song 11':{'score': 0.4}}}]
        self.assertEqual(0.0, TextToPlayList._getWeightedScore(resultList))
        resultList = [{'tracks':{'song 1':{'score': 1.0}, 'song 11':{'score': 0.8}}}, 
                      {'topmatch':'song 2', 'tracks':{'song 2':{'score': 0.86}, 'song 21':{'score': 0.8}}}]
        self.assertEqual(0.43, round(TextToPlayList._getWeightedScore(resultList), 2))
        resultList = [{'topmatch':'song 1', 'tracks':{'song x':{'score': 0.0}}}, 
                      {'topmatch':'song 2', 'tracks':{'song 2':{'score': 0.4}}}]
        self.assertEqual(0.20, round(TextToPlayList._getWeightedScore(resultList), 2))
        resultList = [{'topmatch':'song 1', 'tracks':{'song 1':{}, 'song 11':{'score': 0.8}}}, 
                      {'topmatch':'song 2', 'tracks':{'song x':{'score': 0.8}, 'song 21':{'score': 0.7}}}]
        self.assertEqual(0.0, round(TextToPlayList._getWeightedScore(resultList), 2))
        
    def _executeTestFlow(self, textToPlayList, timeout):
        stopThread= threading.Event()
        queue = Queue.Queue()
        result = None
        t1 = threading.Thread(target=textToPlayList.createPlayList, \
                              args=("If I can't let it go out of my mind", ' ', queue, stopThread))
        t1.start()
        initTime = time.time()
        while True:
            currentTime = time.time()
            try:
                result = queue.get(block = True, timeout = timeout - (currentTime - initTime))
                print result
                queue.task_done()                
            except Queue.Empty:
                stopThread.set()
                break                                    
            if result['score'] >= 0.99 or currentTime - initTime >= timeout:
                stopThread.set()
                break            
        t1.join()
        while not queue.empty():
            result = queue.get_nowait()            
#             print result
            queue.task_done()
        return result
            
    def test_createPlaylist(self):
        api = SpotifyMetaStoreAPI(self.db)
        m = TextToPlayList(self.db, api)
        self.assertNotEqual(None, self._executeTestFlow(m, 6))
        self.assertNotEqual(None, self._executeTestFlow(m, 2))
        self.assertNotEqual(None, self._executeTestFlow(m, 2))
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()