'''
Created on Aug 10, 2013

@author: ehsan
'''
import unittest
from playlistmaker.util.ratelimit import RateLimiter

class TestRateLimit(unittest.TestCase):

    def testCreationSameID(self):
        r1 = RateLimiter('{0}-id1'.format(self._testMethodName), 5)
        self.assertEqual(5, r1.rate)        
        r2 = RateLimiter('{0}-id1'.format(self._testMethodName), 3)
        self.assertEqual(5, r2.rate)
        self.assertEqual(5, r1.rate)
    
    def testCreationDifferntID(self):
        r1 = RateLimiter('{0}-id1'.format(self._testMethodName), 5)
        self.assertEqual(5, r1.rate)        
        r2 = RateLimiter('{0}-id2'.format(self._testMethodName), 3)
        self.assertEqual(3, r2.rate)
        self.assertEqual(5, r1.rate)
    
    def testCreationMixID(self):
        r1 = RateLimiter('{0}-id1'.format(self._testMethodName), 5)
        self.assertEqual(5, r1.rate)        
        r2 = RateLimiter('{0}-id2'.format(self._testMethodName), 3)
        self.assertEqual(3, r2.rate)
        self.assertEqual(5, r1.rate)
        r1 = RateLimiter('{0}-id1'.format(self._testMethodName), 8)
        self.assertEqual(5, r1.rate)
    
    def testRateLimitBelow(self):
        r = RateLimiter('{0}-id1'.format(self._testMethodName), 5)
        for _ in range(4):
            r.do()
            
    def testRateLimitAbove(self):
        
        r = RateLimiter('{0}-id1'.format(self._testMethodName), 2)
        for _ in range(8):            
            r.do()            
            self.assertTrue(r.count <= 2)            
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()