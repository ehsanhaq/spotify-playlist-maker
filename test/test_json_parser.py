'''
Created on Aug 16, 2013

@author: ehsan
'''
import unittest
from playlistmaker.spotifyapi.json_parser import JSONTrackListParser
import ast


class TestJSONParser(unittest.TestCase):

    def testHeaderWithEmptyTrackList(self):
        jsonStr = '{ "info" : {"limit" : 100, "num_results" : 7093, "offset" : 0, \
                               "page" : 1, "query" : "let it go", "type" : "track"}, \
                   "tracks" :[]}' 
        parser = JSONTrackListParser(jsonStr)
        expected = {'header': {'query': u'let it go', 'total': 7093, 'limit': 100, 'page': 1, 'offset': 0}, \
                    'tracks': {}}
        self.assertEqual(expected, parser.parsed)
    
    def testHeaderWithDuplicateTracks(self):
        jsonStr = '{ "info" : { "limit" : 100, "num_results" : 7093, "offset" : 0, \
                              "page" : 1, "query" : "let it go", "type" : "track"}, \
                   "tracks" :[{"href" : "href1", "length" : 197.4978, "name" : "Song 1", "popularity" : "0.62"}, \
                              {"href" : "href2", "length" : 197.4978, "name" : "Song 3", "popularity" : "0.62"}, \
                              {"href" : "href3", "length" : 197.4978, "name" : "Song 1", "popularity" : "0.69"}]}' 
        parser = JSONTrackListParser(jsonStr)
        expectedHeader = {'query': u'let it go', 'total': 7093, 'limit': 100, 'page': 1, 'offset': 0}
        self.assertEqual(expectedHeader, parser.parsed['header'])
        expectedTracks = {'song 1': {'length': 197.4978, 'href': 'href3', 'name': 'Song 1', 'popularity': 0.69}, \
                          'song 3': {'length': 197.4978, 'href': 'href2', 'name': 'Song 3', 'popularity': 0.62}}
        self.assertEqual(expectedTracks, parser.parsed['tracks'])
        
    def testWithActualDataJSON(self):       
        parser = JSONTrackListParser(open('data/sample.json').read())        
        expected = ast.literal_eval(open('data/expected_sample_json.txt').read())        
        self.assertEqual(expected, parser.parsed)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()