'''
Created on Aug 9, 2013

@author: ehsan
'''
import unittest
from playlistmaker.mq.snake_mq import SnakeMQ


class TestSnakeMQ(unittest.TestCase):

    def setUp(self):
        self.mq = SnakeMQ('localhost', 5559, 5560)

    def tearDown(self):
        self.mq.tearDown()

    def testSendReceive1(self):        
        self.mq.getEndPoint1().write('message1')
        msg = self.mq.getEndPoint2().read(timeout=1)
        self.assertTrue(msg is None or msg == 'message1')
     
    def testSendReceive2(self):        
        self.mq.getEndPoint2().write('message1')
        msg = self.mq.getEndPoint1().read(timeout=1)
        self.assertTrue(msg is None or msg == 'message1')
    
    def testSendReceive3(self):        
        self.mq.getEndPoint1().write('message1')        
        msg = self.mq.getEndPoint2().read(timeout=1)        
        self.assertTrue(msg is None or msg == 'message1')
        self.mq.getEndPoint2().write('message2')
        msg = self.mq.getEndPoint1().read(timeout=1)        
        self.assertTrue(msg is None or msg == 'message2')
        self.mq.getEndPoint1().write('message11')        
        msg = self.mq.getEndPoint2().read(timeout=1)
        self.assertTrue(msg is None or msg == 'message11')
        self.mq.getEndPoint2().write('message22')
        self.mq.getEndPoint2().write('message222')
        msg = self.mq.getEndPoint1().read(timeout=1)   
        self.assertEqual('message22', msg)
        msg = self.mq.getEndPoint1().read(timeout=1)   
        self.assertEqual('message222', msg)
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()