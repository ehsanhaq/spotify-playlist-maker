'''
Created on Aug 4, 2013

@author: ehsan
'''
import unittest
from playlistmaker.datastore.memcached_store import MemcacheStore
import time
import tempfile
import os
import cPickle

class TestMemCacheDBImplementation(unittest.TestCase):

    def setUp(self):        
        self.db = MemcacheStore(['127.0.0.1'], flush = True)
        
    def tearDown(self):
        pass
         
    def testDBCreate(self):
        self.assertNotEqual(None, self.db)
     
    def testSimple(self):
        self.assertFalse(self.db.exists('key'))
        self.assertEqual(None, self.db.get('key'))
        self.db.put('key', 'value')
        self.assertTrue(self.db.exists('key'))
        self.assertEqual('value', self.db.get('key'))
        self.db.delete('key')
        self.assertFalse(self.db.exists('key'))
        
    def testOverwrite(self):
        self.db.put('key', 'value1')
        self.db.put('key', 'value2')
        self.assertEqual('value2', self.db.get('key'))
        
    def testBatch(self):
        self.db.batchWrite([('key1','value1'), ('key2', 'value2'), ('key3', 'value3')])
        self.assertEqual('value1', self.db.get('key1'))
        self.assertEqual('value2', self.db.get('key2'))
        self.assertEqual('value3', self.db.get('key3'))
        
    def testStoreObjects(self):
        d = {'1': 'ehsan', '2': 'inam'}
        self.db.put('key', d)
        self.assertEqual(d, self.db.get('key'))
        
    def testExpiration(self):
        self.db.put('key', 'value', 1)
        time.sleep(2)
        self.assertFalse(self.db.exists('key'))
    
    def testUTF8(self):
        self.db.put('key', 'Ivan Krsti\xc4\x87')        
        self.assertEqual('Ivan Krsti\xc4\x87', self.db.get('key'))            
        self.db.put('Ivan Krsti\xc4\x87', 'Ivan Krsti\xc4\x87')
        self.assertEqual('Ivan Krsti\xc4\x87', self.db.get('Ivan Krsti\xc4\x87'))
    
    def testVeryLongKey(self):
        key = ''.join(['This_is_a_very_long_key_This_is_a_very_long_key_This_is_a_very_long_key' for _ in range(50)])
#         print key              
        self.db.put(key, 'small value')        
        self.assertEqual('small value', self.db.get(key))
        
    def testDumpFile(self):
        self.db = MemcacheStore(['127.0.0.1'], flush = False)
        self.db.put('key1', 'value1', 100)
        self.db.put('key2', 'value2', 700)
        self.db.put('key3', 'value3', 60)
    
    def testWithControlCharacterInKey(self):
        self.db.put('key with space', 'value df f')
        self.db.put('key\r', 'value df f')
        self.assertEqual('value df f', self.db.get('key\r'))
        self.db.put('key\0', 'value df g')
        self.assertEqual('value df g', self.db.get('key\0'))
        # Now previous record with key 'key\r' was overwritten  
        self.assertEqual('value df f', self.db.get('key\r'))
    
    def testWithMoreThan30DayExpiry(self):
        self.db.put('key', 'value df f',31359535)
        self.assertTrue(self.db.exists('key'))
        self.assertEqual('value df f', self.db.get('key'))
    
    def testSaveToFile(self):
        self.db.put('key', 'value')
        self.db.put('key1', 'value1')
        self.db.put('key2', 'value2')
        self.db.put('key3', 'value3 with spaces')
        self.db.put('key4', {1:'ehsan', 2:'iname'})
        self.db.save('tmp')
        f = open('tmp', 'rb')
        actual = {}
        while True:
            try:
                record = cPickle.load(f)
            except EOFError:
                break
            actual[record['key']] = record['value']['v']
        expected = {'key3': 'value3 with spaces', 'key2': 'value2', 'key1': 'value1', 'key': 'value', \
                    'key4': {1: 'ehsan', 2: 'iname'}}                    
        self.assertItemsEqual(expected, actual)
        os.remove('tmp')
    
    def testLoadFromFile(self):
        (_, fName) = tempfile.mkstemp()
        self.db.put('key', 'value')
        self.db.put('key1', 'value1')
        self.db.put('key2', 'value2')
        self.db.put('key3', 'value3 with spaces')
        self.db.put('key4', {1:'ehsan', 2:'iname'})
        self.db.put('Ivan Krsti\xc4\x87', 'Ivan Krsti\xc4\x87')
        self.db.save(fName)
        self.db = MemcacheStore(['127.0.0.1'], flush = True)        
        self.db.load(fName)
        os.remove(fName)
        self.assertEqual('value', self.db.get('key'))
        self.assertEqual('value1', self.db.get('key1'))
        self.assertEqual('value2', self.db.get('key2'))
        self.assertEqual('value3 with spaces', self.db.get('key3'))
        self.assertEqual({1:'ehsan', 2:'iname'}, self.db.get('key4'))
        self.assertEqual('Ivan Krsti\xc4\x87', self.db.get('Ivan Krsti\xc4\x87'))
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()