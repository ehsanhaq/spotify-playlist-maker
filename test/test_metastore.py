'''
Created on Aug 12, 2013

@author: ehsan
'''
import unittest
from playlistmaker.spotifyapi.metastore import SpotifyMetaStoreAPI
from playlistmaker.datastore.leveldb_store import LevelDBStore
import tempfile
from mock import MagicMock
from dateutil import tz
import datetime as dt
from datetime import datetime, date
from playlistmaker.spotifyapi import metastore
from urllib2 import HTTPError
import urllib2
from playlistmaker.datastore.memcached_store import MemcacheStore

urllib_open_originial = urllib2.urlopen

def makeMokedResponse(url=None, headers=None, data=None):
    response = MagicMock()
    response.headers = MagicMock()
    response.geturl = MagicMock(return_value=url)
    response.headers.headers = headers
    response.read = MagicMock(return_value=data)
    return response
    
class TestSpotifyMetaStoreBase(object):
            
    def _quoteSearchString(self, searchText):
        return urllib2.quote(searchText)
    
    def _extractHeaderFromSampleFile(self, fileName):
        f = open(fileName)
        headerData = f.read()
        headerList = headerData.split('\n')
        return SpotifyMetaStoreAPI._extractHeadersFromList(headerList)
            
    def _getHeadersFromDict(self, headerDict):
        headers = []
        for key, value in headerDict.iteritems():
            headers.append(''.join([key, ': ', value]))
        return headers
    
    def testConvertToLocalTZ(self):
        api = SpotifyMetaStoreAPI(self.db)
        converted = api._changeTimeZone('Mon, 12 Aug 2013 19:36:52 GMT', tz.gettz('GMT'))
        actual = datetime.strftime(converted, metastore.API_TIME_FORMAT)
        self.assertEqual('Mon, 12 Aug 2013 19:36:52 GMT', actual)
        converted = api._changeTimeZone('Mon, 12 Aug 2013 19:36:52 UTC', tz.gettz('GMT'))
        actual = datetime.strftime(converted, metastore.API_TIME_FORMAT)
        # Because CET timezone is not recognized by some system.
#         self.assertEqual('Mon, 12 Aug 2013 19:36:52 GMT', actual)
#         converted = api._changeTimeZone('Mon, 12 Aug 2013 19:36:52 CET', tz.gettz('GMT'))
#         actual = datetime.strftime(converted, metastore.API_TIME_FORMAT)
        self.assertEqual('Mon, 12 Aug 2013 17:36:52 GMT', actual)
        converted = api._changeTimeZone('Mon, 12 Aug 2013 19:36:52 GMT', tz.gettz('Europe/Stockholm'))
        actual = datetime.strftime(converted, metastore.API_TIME_FORMAT)
        self.assertEqual('Mon, 12 Aug 2013 21:36:52 CEST', actual)        
    
    def testExtractHeaders(self):
        api = SpotifyMetaStoreAPI(self.db)
        headers = ['header1: value1\r\r\n', 'header2:value2\n\r', 'header3 :value3', \
                   'header4 : value4\r\n', 'header5: val:u:e5', 'header6:value6\r\n\r\n']
        response = makeMokedResponse(headers=headers)        
        expected = {'header2': 'value2', 'header3': 'value3', 'header1': 'value1', \
                    'header4': 'value4', 'header5': 'val:u:e5', 'header6': 'value6'}        
        self.assertEqual(expected, api._extractHeaders(response))            
    
    def testpackResultIntoDictionary(self):
        api = SpotifyMetaStoreAPI(self.db)
        response = makeMokedResponse(url='http://ws.spotify.com/search/1/track.json?', \
                                          headers = ['header1: value1\r\r\n'], \
                                          data = 'data')        
        expected = {'url': 'http://ws.spotify.com/search/1/track.json?', 'headers': {'header1': 'value1'}, \
                    'data': 'data', 'format': 'json'}
        self.assertEqual(expected, api._packResultIntoDictionary(response))
        response = makeMokedResponse(url='http://ws.spotify.com/search/1/track?', \
                                          headers=[], data='data')        
        expected = {'url': 'http://ws.spotify.com/search/1/track?', 'headers': {}, 'data': 'data', 'format': 'xml'}
        self.assertEqual(expected, api._packResultIntoDictionary(response))
    
    def testCacheSearchResult(self):
        api = SpotifyMetaStoreAPI(self.db)
        headers = ['Expires: Mon, 12 Aug ' + str(date.today().year+1) + ' 16:40:52 GMT']
        response = makeMokedResponse(url='http://ws.spotify.com/search/1/track.json?', \
                                          headers=headers, data='data')        
        expected = api._packResultIntoDictionary(response)
        api._cacheSearchResult('key', expected)
        actual = self.db.get('key')
        self.assertEqual(expected, actual)
    
    def testSearchFromCacheRecordNotExist(self):
        api = SpotifyMetaStoreAPI(self.db)        
        actual = api._searchFromCache('key')
        self.assertEqual(None, actual)
    
    def testSearchFromCacheGoodRecord(self):
        api = SpotifyMetaStoreAPI(self.db)
        headers=['Expires: Mon, 12 Aug ' + str(date.today().year+1) + ' 16:40:52 GMT']
        response = makeMokedResponse(url='http://ws.spotify.com/search/1/track.json?', \
                                          headers=headers, data='data')        
        expected = api._packResultIntoDictionary(response)
        api._cacheSearchResult('key', expected)
        actual = api._searchFromCache('key')
        self.assertEqual(expected, actual)        
    
    def testSearchRecordExistsInCache(self):
        api = SpotifyMetaStoreAPI(self.db)
        urllib2.urlopen = MagicMock(side_effect=RuntimeError('Should not call urlopen.'))
        # For XML and Page 1        
        headers=['Expires: Mon, 12 Aug ' + str(date.today().year+1) + ' 16:40:52 GMT']
        response = makeMokedResponse(url='http://ws.spotify.com/search/1/track?q=I%20believe%you', \
                                          headers=headers, data='')
        expected = api._packResultIntoDictionary(response)        
        api._cacheSearchResult('I%20believe%20you1xml'.lower(), expected)        
        actual = api.search('I believe you')
        self.assertEqual(expected, actual)
        # For XML and Page 2        
        response = makeMokedResponse(url='http://ws.spotify.com/search/1/track?q=I%20believe%you&page=2', \
                                          headers=headers, data='')
        expected = api._packResultIntoDictionary(response)
        api._cacheSearchResult(self._quoteSearchString('I believe you2xml'.lower()), expected)
        actual = api.search('I believe you', 2)
        self.assertEqual(expected, actual)
        # For JSON and Page 1
        response = makeMokedResponse(url='http://ws.spotify.com/search/1/track.json?q=I%20believe%you', \
                                          headers=headers, data='')
        expected = api._packResultIntoDictionary(response)
        api._cacheSearchResult(self._quoteSearchString('I believe you1json'.lower()), expected)
        actual = api.search('I believe you', responseFormat='json')        
        self.assertEqual(expected, actual)
        # For JSON and Page 2
        response = makeMokedResponse(url='http://ws.spotify.com/search/1/track.json?q=I%20believe%you&page=2', \
                                          headers=headers, data='')
        expected = api._packResultIntoDictionary(response)
        api._cacheSearchResult(self._quoteSearchString('I believe you2json'.lower()), expected)
        actual = api.search('I believe you', page=2, responseFormat='json')
        self.assertEqual(expected, actual)                                
    
    def testSearchRecordNotFound(self):
        api = SpotifyMetaStoreAPI(self.db)
        headers=['Expires: Mon, 12 Aug ' + str(date.today().year-1) + ' 16:40:52 GMT', \
                 'Last-Modified: Mon, 10 Aug ' + str(date.today().year-1) + ' 16:40:52 GMT']
        url='http://ws.spotify.com/search/1/track?q=I%20believe%you'        
        response = makeMokedResponse(url, headers=headers, data='')
        urllib2.urlopen = MagicMock(return_value=response)        
        actual = api.search('I believe you')
        self.assertEqual(response.geturl(), actual['url'])        
        self.assertEqual(api._extractHeaders(response), actual['headers'])
    
    def testRepeatedSearchWithActualXMLData(self):
        api = SpotifyMetaStoreAPI(self.db)
        headerDict = self._extractHeaderFromSampleFile('data/header_json.txt')                
        newExpire = datetime.strftime(datetime.now()+dt.timedelta(1,0), metastore.API_TIME_FORMAT) + 'GMT'
        headerDict['Expires'] = newExpire
        headers = self._getHeadersFromDict(headerDict);
        url = headerDict['Request URL'] 
        response = makeMokedResponse(url, headers=headers, data=open('data/sample.json').read())
        urllib2.urlopen = MagicMock(return_value=response)
        resp1 = api.search('let it go')
        resp2 = api.search('let it go')
        self.assertEqual(resp1, resp2)
        self.assertEqual(1, (urllib2.urlopen).call_count)

class TestSpotifyMetaStoreLevelDB(TestSpotifyMetaStoreBase, unittest.TestCase):
    def setUp(self):
        self.db = LevelDBStore(tempfile.mkdtemp())

    def tearDown(self):
        urllib2.urlopen = urllib_open_originial                    

    def testSearchFromCacheExpiredRecord(self):
        api = SpotifyMetaStoreAPI(self.db)
        expiredTime = datetime.strftime(datetime.now(), '%a, %d %b %Y %H:%M:%S ') + 'GMT'
        l = expiredTime.split()
        l[3] = str(int(l[3])-1)
        expiredTime = ' '.join(l)
        headers = ['Expires: ' + expiredTime, 'Last-Modified: ' + expiredTime]
        url = 'http://ws.spotify.com/search/1/track.json?'
        response = makeMokedResponse(url=url, headers=headers, data='data')                
        expected = api._packResultIntoDictionary(response)
        api._cacheSearchResult('key', expected)        
        urllib2.urlopen = MagicMock(side_effect=HTTPError(url, 304, 'Not Modified', headers, None))        
        actual = api._searchFromCache('key')
        self.assertEqual(expected, actual)
        self.assertTrue((urllib2.urlopen).called)
        self.assertEqual(1, (urllib2.urlopen).call_count)
        
    def testSearchRecordStaleInCacheNotModified(self):
        api = SpotifyMetaStoreAPI(self.db)        
        headers=['Expires: Mon, 12 Aug ' + str(date.today().year-1) + ' 16:40:52 GMT', \
                 'Last-Modified: Mon, 10 Aug ' + str(date.today().year-1) + ' 16:40:52 GMT']
        url='http://ws.spotify.com/search/1/track?q=I%20believe%you'
        urllib2.urlopen = MagicMock(side_effect=HTTPError(url, 304, 'Not Modified', headers, None))
        response = makeMokedResponse(url, headers=headers, data='')
        expected = api._packResultIntoDictionary(response)
        api._cacheSearchResult(self._quoteSearchString('I believe you1xml'.lower()), expected)
        actual = api.search('I believe you')
        self.assertEqual(expected, actual)
    
    def testSearchRecordStaleInCacheModified(self):
        api = SpotifyMetaStoreAPI(self.db)
        headers=['Expires: Mon, 12 Aug ' + str(date.today().year-1) + ' 16:40:52 GMT', \
                 'Last-Modified: Mon, 10 Aug ' + str(date.today().year-1) + ' 16:40:52 GMT']
        url='http://ws.spotify.com/search/1/track?q=I%20believe%you'        
        response = makeMokedResponse(url, headers=headers, data='')
        urllib2.urlopen = MagicMock(return_value=response)
        expected = api._packResultIntoDictionary(response)
        api._cacheSearchResult('I believe you1xml', expected)
        actual = api.search('I believe you')
        self.assertEqual(expected, actual)

class TestSpotifyMetaStoreMemCachedDB(TestSpotifyMetaStoreBase, unittest.TestCase):
    def setUp(self):
        self.db = MemcacheStore(['127.0.0.1'], flush = True)        

    def tearDown(self):
        urllib2.urlopen = urllib_open_originial        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()