'''
Created on Jul 28, 2013

@author: ehsan
'''
import unittest
import tempfile
from playlistmaker.datastore.codernity_store import CodernityDBStore
import collections

class TestCodernityDBImplementation(unittest.TestCase):
 
    def setUp(self):        
        self.db = CodernityDBStore(tempfile.mkdtemp())
    def tearDown(self):
        pass
         
    def testDBCreate(self):        
        self.assertNotEqual(None, self.db)
     
    def testSimple(self):
        self.assertFalse(self.db.exists('key'))
        self.assertEqual(None, self.db.get('key'))
        self.db.put('key', 'value')
        self.assertTrue(self.db.exists('key'))
        self.assertEqual('value', self.db.get('key'))
        self.db.delete('key')
        self.assertFalse(self.db.exists('key'))
    
    def testOverwrite(self):
        self.db.put('key', 'value1')
        self.db.put('key', 'value2')
        self.assertEqual('value2', self.db.get('key'))
        
    def testBatch(self):
        self.db.batchWrite([('key1','value1'), ('key2', 'value2'), ('key3', 'value3')])
        self.assertEqual('value1', self.db.get('key1'))
        self.assertEqual('value2', self.db.get('key2'))
        self.assertEqual('value3', self.db.get('key3'))
    
    
    def testIter(self):
        self.db.batchWrite([('key01','value01'), ('key02', 'value02'), ('key03', 'value03'),
                            ('key04','value04'), ('key05', 'value05'), ('key06', 'value06'),
                            ('key07','value07'), ('key08', 'value08'), ('key09', 'value09')])
        expected = collections.deque([('key03','value03'), ('key04','value04'), ('key05','value05'),
                    ('key06','value06'), ('key07','value07'), ('key08','value08')])
        for actual in self.db.getIterator('key03', 'key08'):
            self.assertEqual(actual, expected.popleft())
        self.assertEqual(collections.deque([]), expected)
        
        expected = collections.deque([])
        for actual in self.db.getIterator('key03', 'key02'):
            self.assertEqual(actual, expected.popleft())
        self.assertEqual(collections.deque([]), expected)
        
        expected = collections.deque([('key01','value01')])
        for actual in self.db.getIterator('key00', 'key01'):
            self.assertEqual(actual, expected.popleft())
        self.assertEqual(collections.deque([]), expected)
        
        expected = collections.deque([('key09','value09')])
        for actual in self.db.getIterator('key09', 'key099'):
            self.assertEqual(actual, expected.popleft())
        self.assertEqual(collections.deque([]), expected)
    