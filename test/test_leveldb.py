'''
Created on Jul 27, 2013

@author: ehsan
'''
import unittest
from playlistmaker.datastore.leveldb_store import LevelDBStore
import tempfile
import collections
import time
import cPickle
import os
 
class TestLevelDBImplementation(unittest.TestCase):
 
    def setUp(self):        
        self.db = LevelDBStore(tempfile.mkdtemp())
    def tearDown(self):
        pass
         
    def testDBCreate(self):
        self.assertNotEqual(None, self.db)
     
    def testSimple(self):
        self.assertFalse(self.db.exists('key'))
        self.assertEqual(None, self.db.get('key'))
        self.db.put('key', 'value')
        self.assertTrue(self.db.exists('key'))
        self.assertEqual('value', self.db.get('key'))
        self.db.delete('key')
        self.assertFalse(self.db.exists('key'))
    
    def testOverwrite(self):
        self.db.put('key', 'value1')
        self.db.put('key', 'value2')
        self.assertEqual('value2', self.db.get('key'))
        
    def testBatch(self):
        self.db.batchWrite([('key1','value1'), ('key2', 'value2'), ('key3', 'value3')])
        self.assertEqual('value1', self.db.get('key1'))
        self.assertEqual('value2', self.db.get('key2'))
        self.assertEqual('value3', self.db.get('key3'))
    
    def testStoreObjects(self):
        d = {'1': 'ehsan', '2': 'inam'}
        self.db.put('key', d)
        self.assertEqual(d, self.db.get('key'))
    
    def testExpiration(self):
        self.db.put('key', 'value', 1)
        time.sleep(2)
        self.assertFalse(self.db.exists('key'))
    
    def testStale(self):
        self.db.put('key', 'value', 1)
        time.sleep(2)
        self.assertEqual((False, 'value'), self.db.get('key',True))
    
    def testUTF8(self):
        self.db.put('key', 'Ivan Krsti\xc4\x87')        
        self.assertEqual('Ivan Krsti\xc4\x87', self.db.get('key'))
        self.db.put('Ivan Krsti\xc4\x87', 'Ivan Krsti\xc4\x87')
        self.assertEqual('Ivan Krsti\xc4\x87', self.db.get('Ivan Krsti\xc4\x87'))
    
    def testVeryLongKey(self):
        key = ''.join(['This_is_a_very_long_key_This_is_a_very_long_key_This_is_a_very_long_key' for _ in range(50)])
        self.db.put(key, 'small value')
        self.assertEqual('small value', self.db.get(key))
        
    def testIter(self):
        self.db.batchWrite([('key01','value01'), ('key02', 'value02'), ('key03', 'value03'),
                            ('key04','value04'), ('key05', 'value05'), ('key06', 'value06'),
                            ('key07','value07'), ('key08', 'value08'), ('key09', 'value09')])
        expected = collections.deque([('key03','value03'), ('key04','value04'), ('key05','value05'),
                    ('key06','value06'), ('key07','value07'), ('key08','value08')])
        for actual in self.db.getIterator('key03', 'key08'):
            self.assertEqual(actual, expected.popleft())
        self.assertEqual(collections.deque([]), expected)
        
        expected = collections.deque([])
        for actual in self.db.getIterator('key03', 'key02'):
            self.assertEqual(actual, expected.popleft())
        self.assertEqual(collections.deque([]), expected)
        
        expected = collections.deque([('key01','value01')])
        for actual in self.db.getIterator('key00', 'key01'):
            self.assertEqual(actual, expected.popleft())
        self.assertEqual(collections.deque([]), expected)
        
        expected = collections.deque([('key09','value09')])
        for actual in self.db.getIterator('key09', 'key099'):
            self.assertEqual(actual, expected.popleft())
        self.assertEqual(collections.deque([]), expected)
        
        expected = [('key01','value01'), ('key02', 'value02'), ('key03', 'value03'),
                    ('key04','value04'), ('key05', 'value05'), ('key06', 'value06'),
                    ('key07','value07'), ('key08', 'value08'), ('key09', 'value09')]
        actual = []
        for rec in self.db.getIterator():
            actual.append(rec)
        self.assertItemsEqual(expected, actual)        
    
    def testSaveToFile(self):
        self.db.put('key', 'value')
        self.db.put('key1', 'value1')
        self.db.put('key2', 'value2')
        self.db.put('key3', 'value3 with spaces')
        self.db.put('key4', {1:'ehsan', 2:'iname'})
        self.db.save('tmp')        
        f = open('tmp', 'rb')
        actual = {}
        while True:
            try:
                record = cPickle.load(f)
            except EOFError:
                break
            actual[record['key']] = record['value']['v']
        expected = {'key3': 'value3 with spaces', 'key2': 'value2', 'key1': 'value1', 'key': 'value', \
                    'key4': {1: 'ehsan', 2: 'iname'}}                    
        self.assertItemsEqual(expected, actual)
        os.remove('tmp')
    
    def testLoadFromFile(self):
        (_, fName) = tempfile.mkstemp()
        self.db.put('key', 'value')
        self.db.put('key1', 'value1')
        self.db.put('key2', 'value2')
        self.db.put('key3', 'value3 with spaces')
        self.db.put('key4', {1:'ehsan', 2:'iname'})
        self.db.put('Ivan Krsti\xc4\x87', 'Ivan Krsti\xc4\x87')
        self.db.save(fName)
        db = LevelDBStore(tempfile.mkdtemp())
        db.load(fName)
        os.remove(fName)
        self.assertEqual('value', db.get('key'))
        self.assertEqual('value1', db.get('key1'))
        self.assertEqual('value2', db.get('key2'))
        self.assertEqual('value3 with spaces', db.get('key3'))
        self.assertEqual({1:'ehsan', 2:'iname'}, db.get('key4'))
        self.assertEqual('Ivan Krsti\xc4\x87', self.db.get('Ivan Krsti\xc4\x87'))
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
