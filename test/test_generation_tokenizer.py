'''
Created on Aug 18, 2013

@author: ehsan
'''
import unittest
from playlistmaker.util.tokenizer import GenerationTokenizer

class TestGenerationTokenizer(unittest.TestCase):

    def testShiftFromLeftToRightSimple(self):
        leftList = []
        rightList = []
        GenerationTokenizer._shiftFromLeftToRight(leftList, rightList, 0)
        self.assertEqual([], leftList)
        self.assertEqual([], rightList)
        
        leftList = [1]
        rightList = [1]
        GenerationTokenizer._shiftFromLeftToRight(leftList, rightList, 0)
        self.assertEqual([1], leftList)
        self.assertEqual([1], rightList)
        
        leftList = [1]
        rightList = [1]
        GenerationTokenizer._shiftFromLeftToRight(leftList, rightList, 1)
        self.assertEqual([], leftList)
        self.assertEqual([1,1], rightList)
        
        leftList = [1]
        rightList = []
        GenerationTokenizer._shiftFromLeftToRight(leftList, rightList, 1)
        self.assertEqual([], leftList)
        self.assertEqual([1], rightList)
        
        leftList = [1,2,3,4,5]
        rightList = [6,7,8]
        GenerationTokenizer._shiftFromLeftToRight(leftList, rightList, 3)
        self.assertEqual([1,2], leftList)
        self.assertEqual([3,4,5,6,7,8], rightList)
    
    def testShiftFromLeftToRightException(self):
        self.assertRaises(ValueError, GenerationTokenizer._shiftFromLeftToRight, [], [], 1)
        self.assertRaises(ValueError, GenerationTokenizer._shiftFromLeftToRight, [1,2], [], 3)
    
    def testShiftFromRightToRLeftSimple(self):
        leftList = []
        rightList = []
        GenerationTokenizer._shiftFromRightToLeft(leftList, rightList, 0)
        self.assertEqual([], leftList)
        self.assertEqual([], rightList)
        
        leftList = [1]
        rightList = [1]
        GenerationTokenizer._shiftFromRightToLeft(leftList, rightList, 0)
        self.assertEqual([1], leftList)
        self.assertEqual([1], rightList)
        
        leftList = [1]
        rightList = [1]
        GenerationTokenizer._shiftFromRightToLeft(leftList, rightList, 1)
        self.assertEqual([1,1], leftList)
        self.assertEqual([], rightList)
        
        leftList = []
        rightList = [1]
        GenerationTokenizer._shiftFromRightToLeft(leftList, rightList, 1)
        self.assertEqual([1], leftList)
        self.assertEqual([], rightList)
        
        leftList = [1,2,3,4,5]
        rightList = [6,7,8,9,10]
        GenerationTokenizer._shiftFromRightToLeft(leftList, rightList, 3)
        self.assertEqual([1,2,3,4,5,6,7,8], leftList)
        self.assertEqual([9,10], rightList)
    
    def testShiftFromRightToLeftException(self):
        self.assertRaises(ValueError, GenerationTokenizer._shiftFromLeftToRight, [], [], 1)
        self.assertRaises(ValueError, GenerationTokenizer._shiftFromLeftToRight, [], [1,2], 3)
        
    def testCreationSplitEqualSize(self):
        tokenizer = GenerationTokenizer('Hello this is a test message', 1, ' ')
        self.assertEqual([['Hello', 'this', 'is'], ['a', 'test', 'message']], tokenizer.tokenLists)
        tokenizer = GenerationTokenizer('Hello this is a test message', 2, ' ')
        self.assertEqual([['Hello', 'this'], ['is', 'a'], ['test', 'message']], tokenizer.tokenLists)
    
    def testCreationSplitUnEqualSize(self):
        tokenizer = GenerationTokenizer('Hello this is a test message of unequal splits', 1, ' ')
        self.assertEqual([['Hello', 'this', 'is', 'a'], ['test', 'message', 'of', 'unequal', 'splits']], \
                         tokenizer.tokenLists)
        tokenizer = GenerationTokenizer('Hello this is a test message of unequal splits', 3, ' ')
        self.assertEqual([['Hello', 'this'], ['is', 'a'], ['test', 'message'], ['of', 'unequal', 'splits']], \
                         tokenizer.tokenLists)
    
    def testCreationSplitException(self):            
        self.assertRaises(ValueError, GenerationTokenizer, 'Hello this is a test message', 0, ' ')
        self.assertRaises(ValueError, GenerationTokenizer, 'Hello this is a test message', -1, ' ')
    
    def testShuffleFirstGeneration(self):
        tokenizer = GenerationTokenizer('Hello this is a test message of unequal splits', 1, ' ', 0.2)        
        self.assertEqual([['Hello', 'this', 'is', 'a'], ['test', 'message', 'of', 'unequal', 'splits']], \
                         tokenizer.tokenListsOrig)
        self.assertEqual([['Hello', 'this', 'is'], ['a', 'test', 'message', 'of', 'unequal', 'splits']], \
                         tokenizer.tokenLists)
        # repetative call of has_more_shuffles() should not change the state of the tokenizer.
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertEqual(0, tokenizer.next_shuffle()) # First shuffle between lists at 0 and 1 indexes.
        self.assertEqual([['Hello', 'this', 'is', 'a'], ['test', 'message', 'of', 'unequal', 'splits']], \
                         tokenizer.tokenLists)
        # repetative call of has_more_shuffles() should not change the state of the tokenizer.
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertEqual(0, tokenizer.next_shuffle()) # First shuffle between lists at 0 and 1 indexes.        
        self.assertEqual([['Hello', 'this', 'is', 'a', 'test'], ['message', 'of', 'unequal', 'splits']], \
                         tokenizer.tokenLists)
        # No more shuffles left.
        self.assertFalse(tokenizer.has_more_shuffles())
        self.assertFalse(tokenizer.has_more_shuffles())
        self.assertRaises(StopIteration, tokenizer.next_shuffle)
        # original stays intact.
        self.assertEqual([['Hello', 'this', 'is', 'a'], ['test', 'message', 'of', 'unequal', 'splits']], \
                         tokenizer.tokenListsOrig)
    
    def testShuffleSecondGeneration(self):
        tokenizer = GenerationTokenizer('Hello this is a test message of unequal splits', 2, ' ', 0.2)        
        self.assertEqual([['Hello', 'this', 'is'], ['a', 'test', 'message'], ['of', 'unequal', 'splits']], \
                         tokenizer.tokenListsOrig)
        self.assertEqual([['Hello', 'this'], ['is', 'a', 'test', 'message'], ['of', 'unequal', 'splits']], \
                         tokenizer.tokenLists)
        # repetative call of has_more_shuffles() should not change the state of the tokenizer.
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertEqual(0, tokenizer.next_shuffle()) # First shuffle between lists at 0 and 1 indexes.
        self.assertEqual([['Hello', 'this', 'is'], ['a', 'test', 'message'], ['of', 'unequal', 'splits']], \
                         tokenizer.tokenLists)
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertEqual(0, tokenizer.next_shuffle()) # First shuffle between lists at 0 and 1 indexes.        
        self.assertEqual([['Hello', 'this', 'is', 'a'], ['test', 'message'], ['of', 'unequal', 'splits']], \
                         tokenizer.tokenLists)
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertEqual(1, tokenizer.next_shuffle()) # Second shuffle between lists at 1 and 2 indexes.        
        self.assertEqual([['Hello', 'this', 'is'], ['a', 'test'], ['message', 'of', 'unequal', 'splits']], \
                         tokenizer.tokenLists)
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertEqual(1, tokenizer.next_shuffle()) # Second shuffle between lists at 1 and 2 indexes.        
        self.assertEqual([['Hello', 'this', 'is'], ['a', 'test', 'message'], ['of', 'unequal', 'splits']], \
                         tokenizer.tokenLists)
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertTrue(tokenizer.has_more_shuffles())
        self.assertEqual(1, tokenizer.next_shuffle()) # Second shuffle between lists at 1 and 2 indexes.        
        self.assertEqual([['Hello', 'this', 'is'], ['a', 'test', 'message', 'of'], ['unequal', 'splits']], \
                         tokenizer.tokenLists)
#         # No more shuffles left.
        self.assertFalse(tokenizer.has_more_shuffles())
        self.assertFalse(tokenizer.has_more_shuffles())
        self.assertRaises(StopIteration, tokenizer.next_shuffle)
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()