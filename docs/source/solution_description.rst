Solution Description
********************

The application for solving the problem mentioned in :ref:`Problem Statement <problem_statement>` is developed as a webservice. The webservice exposes a REST API 
and can be called like `<http://[hostname:port]/make_playlist?input=[input_text_for_which_to_create_playlist]>`_. The REST API returns the result as JSON objects.

Detail of the make_playlist REST API
====================================

Return Format
-------------

The result of the API call is a sequence of JSON message(s) followed by some text messages in between. Each JSON object
contains a track list and a score indicating how close the tracks in the result match with the given input. A score of
0 means a very poor match while a score of 1.0 indicates the highest level of match.

For an *input = If%20I%20can't%20let%20it%20go%20out%20of%20my%20mind* a JSON object with score 0, like

{'tracks': [], **'score': 0**}

indicates that there are no track in the result, which also means no part of the input can be matched with any track. 
This is always the first JSON result returned.

Where as a result of score 1.0, like

{'tracks': [{'track': "If I Can't", 'length': 196.871, 'href': 'spotify:track:6mcu7D7QuABVwUGDwovOEh'}, 
{'track': 'Let It Go', 'length': 197.498, 'href': 'spotify:track:6cWpj6c5pNZYEk5KYjNBji'}, 
{'track': 'Out Of My Mind', 'length': 213.368, 'href': 'spotify:track:3zageWW7Zbqdo0edJI8StA'}], **'score': 1.0**} 

indicates that all the words in the input are matched with the words in the track(s) returned in the *tracks* field of the JSON object.

Example
^^^^^^^
    
The call to the *make_playlist*  
For example here is a result returned from the API for the given URL.

`<http://[hostname:port]/make_playlist?input=If%20I%20can't%20let%20it%20go%20out%20of%20my%20mind>`_

{'tracks': [], 'score': 0}

Improving Search...

{'tracks': [{'track': "If I Can't Let Go", 'length': 187.092, 'href': 'spotify:track:7p2kLkFMKTXTB4ZW93sJXi'}], 'score': 0}

Improving Search...

{'tracks': [{'track': "If I Can't Take It with Me", 'length': 153.209, 'href': 'spotify:track:6XPZoc2bHCcV8y7tEfMMuJ'}, {'track': 'I Go Out of My Mind', 'length': 245.919, 'href': 'spotify:track:21IeGalLre8OiLc0PyrsXR'}], 'score': 0.82}

Improving Search...

{'tracks': [{'track': 'If I Had', 'length': 245.788, 'href': 'spotify:track:0B4CaEJPfXNp972UFFCSUl'}, {'track': "Can't Let It Go", 'length': 233.526, 'href': 'spotify:track:5ymMBU5FMelGU76pmpNgFO'}, {'track': 'Out Of My Mind', 'length': 213.368, 'href': 'spotify:track:3zageWW7Zbqdo0edJI8StA'}], 'score': 0.82}

Improving Search...

{'tracks': [{'track': "If I Can't", 'length': 196.871, 'href': 'spotify:track:6mcu7D7QuABVwUGDwovOEh'}, {'track': 'Let It Go', 'length': 197.498, 'href': 'spotify:track:6cWpj6c5pNZYEk5KYjNBji'}, {'track': 'Out Of My Mind', 'length': 213.368, 'href': 'spotify:track:3zageWW7Zbqdo0edJI8StA'}], 'score': 1.0}

Improving Search...

Search Finished

Searching again may improve results!!

Case Insensitive
----------------

The API is case insensitive meaning, an input value of *If I can't Let IT go Out of My Mind* is same as *if i can't let it go out of my mind*

UTF-8
-----

The input is assumed to be in UTF-8 encoding.
  
Iterative improval of results
-----------------------------

The results returned as a sequence of JSON objects are improvd iteratively, each time when a new JSON object 
is returned in a call to *make_playlist* the score is higher than the score in the last JSON object received in the same call.

Termination Criteria
--------------------

There are two termination criteria. The call terminates as soon as one of them occurs.

Best result is found
^^^^^^^^^^^^^^^^^^^^

The call terminates once a result with score 1.0 is found.

Maximum time has expired
^^^^^^^^^^^^^^^^^^^^^^^^

Although the results are improved iteratively, however there is an upper limit on the maximum time 
(configurable) in which the call will terminate eventually. Once the time since the call has been
processing exceeds the threshold time the call is terminated.

Small results first
-------------------

The api tries to find the smallest result(s), those with small number of tracks, first.

|

Spotify Metastore API
=====================

The service relies heavily on `Spotify Metadata API <https://developer.spotify.com/technologies/web-api/>`_. The search API 
is used to search arbitrary tracks, the results returned from the search api are then compiled to produce the results of the
*make_playlist* API.

The Metadata API provides two flavours for the result format, XML and JSON. The search API is capable to work with
both the XML and JSON version of Metadata API. By default it uses XML.

|

Web Framework
=============

During the development the service was developed and tested with two python based Web frameworks.

`Bottle <http://bottlepy.org/docs/dev/>`_
-----------------------------------------

`Bottle <http://bottlepy.org/docs/dev/>`_ is a fast, simple and lightweight WSGI micro web-framework for Python. 
This is the default and recommended framework to be used. The iteraive results are implemented using the 
`asynchronous aplication <http://bottlepy.org/docs/dev/async.html>`_.

`Flask <http://flask.pocoo.org/>`_
----------------------------------

`Flask <http://flask.pocoo.org/>`_ is a microframework for Python based on Werkzeug, Jinja 2 and good intentions. 
The current implementation of the service does not provide iterative results rather all the results are returned once.
Either when the best match has been found or maximum time has elapsed.

|

Caching
=======

Since the call to `Spotify Metadata API <https://developer.spotify.com/technologies/web-api/>`_ is expensive and also, there
is a restriction on the number of calls that can be made per second to the metadata api, therefore it is quite important to reuse
the results as much as possible.
 
In order to reuse the results of the Metadata API local caching is implemented, using two key/value storage 
:ref:`Underlying Cache Storage <underlying_storage>`. Both the implementations are almost identical and interchangeable.


Reuse of results across different calls
---------------------------------------
During the processing of a rquest the input text get splitted into small sentences and for each sentence the Metadata API is called
and the result of each call is get cached into the underlying cache DB. In the subsequent requests if the same sentence is
needed to be queried then it is queried against the cache first, if the cache DB already had the results for the key and the result
is not expired/stale then the results from the cache DB are used and no call to the Metadata API is made. 

Remove duplicates with most popular track
-----------------------------------------

The results returned from the Metadata API may have several duplicates (those haviing identical track name). It would be
a waste of caching space if there are duplicate tracks for a search key word(s), since it will occupy more space of the
precious cache, therefore in case if there are duplicate tracks in the result returned from the Metadata API then only the
one with highest popularity will be kept and stored in the cache. 

Cache Expiry
------------

The results in the cache are stored with an expiration. The expiration time is obtained from the Metadata API *Expires* header value,
the value in the cache can be used uptil this expiration time, after the expiration period the results in the cache are
considered stale and will result in Metadata API call.  

If-Modify for stale item search
-------------------------------

For the stale records in cache a search will end up querying the Metadata API. However in reality there may not be any changes, and fetching
the same results again will be inefficient. In order to cope with this the Metadata API is queried with *If-Modified* flag which returns
with a *HTTP 304* if there are not any modifications and the result from the cache is reused.

.. note::

   Caching is only used to store the results of Metadata API, the results produced by the *make_playlist* are not cached, because a playlist
   created by *make_playlist* might consists of several results of the Metada API, with different Expiries. One idea would be to
   use the minimum expiry of the Metadata API as the expiry of the *make_playlist*

|

.. _underlying_storage:

Underlying Cache Storage
========================

The underlying Cache DB is implemented with two different data stores, any one can be used (configurable).

LevelDB
-------

`LevelDB <https://code.google.com/p/leveldb/>`_ is a fast key-value storage library written at Google that provides 
an ordered mapping from string keys to string values.
LevelDB provides local disk storage for storing key/value records. If the service is running on a single machine then
LevelDB can provide cache recovery during failure and recovery of the service.

Memcache
--------

`Memcached <http://memcached.org/>`_ Free & open source, high-performance, distributed memory object caching system.
In comparision to LevelDB memcache is distributed and therefore very much scalable, and since it is an in memory
storage therefore the latency is also quite low and is bounded to the network latency rather than the disk latency in 
case of LevelDB.

Import Export Across Different Storage
--------------------------------------

Both the Cache Storage implementation also implements export to file and import from file, in this way it is possible
to switch a different storage engine with all the cache entries from a previously running instance. 

|

Rate Limiter
============

As the Metada API limits the number of calls made in a second from a single IP, therefore inorder to prevent
the limiting threshold in high loads and getting penalized by the Metadata API, a control mechanism is put in place.
This Control mechanism is general and can be used to limit the rate of any flow to a specified threshold.
In the *make_playlist* service this rate limiter is put on top of any call that out bounds to Metadata API and is
configured with a smaller value of the maximum threshold specified by Metadata API (which is 10 secs in the
documentation). Once the no of calls to outboud Metadata API exceeds a specified value (configurable) in one second.
The Rate Limiter comes into action and artificially suspends the execution for a small period (configurable). In this
way the outbound calls to Metadata API are restricted not to reach the maximum threshold.

|

Algorithm
=========

Algorithmic details are explained in :ref:`Algorithms <algorithms>`
