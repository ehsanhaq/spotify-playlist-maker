.. Spotify Poetic Playlist Maker documentation master file, created by
   sphinx-quickstart on Tue Aug 20 20:56:03 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Spotify Poetic Playlist Maker's documentation!
=========================================================

This is the documentation for the **Spotify Poetic Playlist Maker** |version|, last updated |today|.

Contents:

.. toctree::
   :maxdepth: 2

   introduction
   solution_description
   algorithms
   install
   mini_production
   production
   api

==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

