.. _algorithms:

Algorithms
**********

One of the problems in creating a playlist from a given input is to find a mechanism to split the contigous
words that should be used to search the tracks from cache storage or from the Metadata API. The requirements
that are presented to drive the design of an algorithm are.

* Contigous Splits
------------------

  Although not stated in the problem statement, but implied, is that the input text is splitted such that
  the splits contain contigous words from the input text. The order of occurance of the words should be
  exactly same as in the original split.
  
  For Example, an input of *"If I can't let it go out of my mind"*, can be splitted like
  
  * *"If I can't"*, *"let it go"*, *"out of my mind"*
  * *"If I can't let it go"*, *"out of my mind"*
  * *"If"*, *"I"*, *"can't"*, *"let"*, *"it"*, *"go"*, *"out"*, *"of"*, *"my"*, *"mind"*
  
  But not like
  
  * *"If can't I"*, *"go let it out"*, *"my mind of"*

* Shortest results first.
-------------------------

  This means that lesser number of contigous splits of the input text are better then more number of splits.
  For Example, for an input of *"If I can't let it go out of my mind"*,
  
  * *"If I can't let it go"*, *"out of my mind"*
  * *"If I can't"*, *"let it go"*, *"out of my mind"*
  * *"If"*, *"I"*, *"can't"*, *"let"*, *"it"*, *"go"*, *"out"*, *"of"*, *"my"*, *"mind"*
  
* Scoring results
-----------------

In order to find an optimum/best solution, there should be a way to score the results for comparision. In the
implementation, scoring is done in two levels. On the first level once a query is made to the Metadata API, the
results returned from the Metadata API are scored using Levistiens distance (using python ``difflib`` module).
The tracks returned from the Metadata API for a particular search are compared with the query text and the one
with the highest score is selected. On the second level an aggregate score for the whole input is calculated by
taking a simple average of the scores caclulated in the first level for all the splits of the input.

|

Generation Based Text Splitting
===============================

In generation based text splitting the input gets splitted into a number of splits depending on the generation
number (0 being no split, 1 being two splits, 2 being 3 splits and so on). In addition to the number of splits
a parameterized overlap ratio is also used to get different combinations. The overlap ratio is an estimate
of the number of words at the boundary of a two adjacent splits that can overlap between the splits in different
possibilities.

Example
-------
For the input *"If I can't let it go out of my mind"*.

* For generation = 0, overlap ratio = has no effect.
  
  There is only one possibility, *"If I can't let it go out of my mind"*

* For generation = 1, overlap ratio = 0.2

  generation = 1, indicates there will be two splits and an overlap ratio of 0.2 will be used to calculate
  the number of words that can overlap between the two splits. The formula to obtain number of splits is simple.
  
  .. math::  
   
   overlapingWords = ciel((totalWords / (generation+1)) * overlapRatio)
   
   overlapingWords = ciel(10/2 * 0.2) = 1
  
  Possible splits for the given text are.
     
   * *"If I can't let"*, *"it go out of my mind"*
   
   * *"If I can't let it"*, *"go out of my mind"*
   
   * *"If I can't let it go"*, *"out of my mind"*     

* For generation = 2, overlap ratio = 0.2

  .. math::

   overlapingWords = ciel(10/3 * 0.2) = 1
    
  Possible splits for the given text are.
     
   * *"If I"*, *"can't let it go"*, *"out of my mind"*
   
   * *"If I can't"*, *"let it go"*, *"out of my mind"*
   
   * *"If I can't let"*, *"it go"*, *"out of my mind"*
   
   * *"If I can't"*, *"let it"*, *"go out of my mind"*
   
   * *"If I can't"*, *"let it go"*, *"out of my mind"*   (duplicate)
   
   * *"If I can't"*, *"let it go out"*, *"of my mind"*

Tree Based Text Splitting
=========================

Need to implement.