Installation: Installing and Running Locally
********************************************

Following are the recommended and (tested with) dependencies which are required to setup and run the application in
your own local environment.

Requirements
============

* Operating System

  * Developed and tested on Ubuntu 13.04, but other versions from and above 12.04 should also work.

* Python

  * Developed and tested on Python 2.7

* Python Libraries and Modules

  * The project depends on several other modules and libraries that are not present in a default python installation.
    A list of these libraries is given below with instructions of installation.

Installation
============

Following modules are required to be installed to run the project.

Installing anc Creating Virtual Environment
-------------------------------------------

It is recommended to install and run the application in a virtual environment rather than the global environment. 
Virtual evironment provides a closed environment, which is very helpful to separate any application and their environment
from interfering with other applications.

* To install virtual environment globally

  .. code-block:: console

    $ [sudo] pip install virtualenv
 
* Create a Virtual Environment 

  .. code-block:: console

    $ virtualenv --no-site-packages spotify_playlist

* Using the created virtual environment.

  .. code-block:: console

    $ source spotify_playlist/bin/activate
    
    (spotify_playlist) $

  This will activate the virtual environment, and the rest of the modules that will be installed, will get installed
  only in this virtual environment rather than the global environment.

Cloning the Project Source
--------------------------

   .. code-block:: console
   
      (spotify_playlist) $ git clone https://ehsanhaq@bitbucket.org/ehsanhaq/spotify-playlist-maker.git 

Installing dependent libraries/modules
--------------------------------------

* Python mocking library

  Mock is a library for testing in Python. It allows you to replace parts of your system under test with mock objects and make assertions about how they have been used.
   
   .. code-block:: console
    
    (spotify_playlist) $ pip install -U mock 
   
* LevelDB
   
  Python bindings for leveldb `LevelDB is a fast key-value storage library <https://code.google.com/p/leveldb/>`_
   
   .. code-block:: console
    
    (spotify_playlist) $  pip install -U leveldb
 
 .. note::
   
   You will also need to have the python-dev package(s) installed on the system.
   
   .. code-block:: console
    
    $ [sudo] apt-get install python-dev 

* MemcacheDB
   
  Pure python memcached client1. It is 100% Python interface to the memcached memory cache daemon. It is the client side software which allows storing values in one or more, possibly remote, memcached servers.
   
      * Download Source tar.gz
      
      .. code-block:: console
      
         $ wget https://pypi.python.org/packages/source/p/python-memcached/python-memcached-1.53.tar.gz#md5=89570d26e7e7b15caa668a6b2678bd3c
         
      * Extract Source in a suitable place.
      
      .. code-block:: console
      
         $ tar -zxvf python-memcached-1.53.tar.gz 
      
      * Build and Install.
      
      .. code-block:: console
      
         (spotify_playlist) $  cd python-memcached-1.53
         
         (spotify_playlist) $  python setupy.py install
      
      * Memcached-tools
      
      .. code-block:: console
      
         $ [sudo] apt-get install memcached-tools
         
* Dateutil
   
      * Download Source tar.gz
      
      .. code-block:: console
      
         $ wget https://pypi.python.org/packages/source/p/python-dateutil/python-dateutil-2.1.tar.gz#md5=1534bb15cf311f07afaa3aacba1c028b
         
      * Extract Source in a suitable place.
      
      .. code-block:: console
      
         $ tar -zxvf python-dateutil-2.1.tar.gz
      
      * Build and Install.
      
      .. code-block:: console
      
         (spotify_playlist) $  cd python-dateutil-2.1
         
         (spotify_playlist) $  python setupy.py install
   
* Cython 
   
  Cython is an optimising static compiler for both the Python programming language and the extended Cython programming language (based on Pyrex). It makes writing C extensions for Python as easy as Python itself.
   
   .. code-block:: console
    
    (spotify_playlist) $  easy_install cython
   
* Bottle: Python Web Framework
   
  Bottle is a fast, simple and lightweight WSGI micro web-framework for Python.
   
   .. code-block:: console
    
    (spotify_playlist) $  pip install -U bottle
   
* Gevent
   
  Bottle is a fast, simple and lightweight WSGI micro web-framework for Python.
   
   .. code-block:: console
    
    (spotify_playlist) $  pip install cython -e git://github.com/surfly/gevent.git@1.0rc2#egg=geven
      
    (spotify_playlist) $  pip install cython -e git://github.com/surfly/gevent.git#egg=gevent
      
* Argument Parser
   
  The argparse module makes it easy to write user friendly command line interfaces.
   
   .. code-block:: console
    
    (spotify_playlist) $  pip install -U argparse

Running all the tests
---------------------

* Export PYTHONPATH variable 
   
   .. code-block:: console
    
    (spotify_playlist) $  export PYTHONPATH=[spotify-playlist-maker directory]
      
    (spotify_playlist) $  export PYTHONPATH=$PYTHONPATH:[spotify-playlist-maker/test directory]
   
* Running the test 
   
   .. code-block:: console
    
    (spotify_playlist) $  python test_all.py
     
Starting the server locally
---------------------------

* Export PYTHONPATH variable 
   
   .. code-block:: console
    
    (spotify_playlist) $  export PYTHONPATH=[spotify-playlist-maker directory]
      
    (spotify_playlist) $  export PYTHONPATH=$PYTHONPATH:[spotify-playlist-maker/test directory]
   
* Running the server
   
   .. code-block:: console
    
    (spotify_playlist) $  cd [spotify-playlist-maker/playlistmaker/web directory]
      
    (spotify_playlist) $  python bottle_server.py

Testing with request(s)
-----------------------

* In your browser:

   http://localhost:9001/make_playlist?input=If I can't let it go out of my mind     