Mini deployment
***************

There is a mini deployment of the application to play with.

* A sample Call to Make Playlist

   `If I can't let it go out of my mind <http://192.241.249.54:9001/make_playlist?input=If%20I%20can't%20let%20it%20go%20out%20of%20my%20mind>`_

* Link for Documentation

   `Documentation <http://192.241.249.54:9001/docs>`_