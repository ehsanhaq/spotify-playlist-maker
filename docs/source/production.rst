Deployment in Production
************************

Selection of Web Server
=======================


Data Store
==========


Multiple Instance of the Service Running
========================================


Application Update Policy
=========================


Security
========


Scaling
=======

For Large Number of Concurrent Users
------------------------------------


For Large Amount of Storage
---------------------------

Deployment Diagram
==================
