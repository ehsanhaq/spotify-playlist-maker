'''
Created on Jul 28, 2013

@author: ehsan
'''
from playlistmaker.datastore.kvstore import DataStoreBase
from CodernityDB.database import Database  # @UnresolvedImport
from CodernityDB.tree_index import TreeBasedIndex  # @UnresolvedImport
from CodernityDB.database import RecordNotFound  # @UnresolvedImport
import md5
import struct

class Md5Index(TreeBasedIndex):
    '''
    Tree Based index that uses the md5 of the key for indexing. 
    '''
    def __init__(self, *args, **kwargs):
        kwargs['key_format'] = '32s'
        self.index = args[1]
        super(Md5Index, self).__init__(*args, **kwargs)

    def make_key_value(self, data):
        return md5(data[self.index]).hexdigest(), None

    def make_key(self, key):
        return md5(key).hexdigest()

class VariableLengthKeyIndex(TreeBasedIndex):
    '''
    Tree Based index that uses the variable length keys for indexing. 
    '''
    def __init__(self, *args, **kwargs):
        kwargs['key_format'] = '32s'
        self.index = args[1]
        super(VariableLengthKeyIndex, self).__init__(*args, **kwargs)

    def make_key_value(self, data):
        return struct.pack('32s', data[self.index]), None

    def make_key(self, key):
        return struct.pack('32s', key)
    
class KeyValueIterator:
    def __init__(self, codernity_iter):
        self.iter = codernity_iter
    
    def __iter__(self):
        return self

    def next(self):
        rec = self.iter.next()
        return (rec['doc']['idx1'], rec['doc']['value'])
        
class CodernityDBStore(DataStoreBase):
    '''
    Key Value data store implementation with Codernity DB. 
    '''    
        
    def __init__(self, db_path):
        '''
        Opens the DB and creates a new one if not already created.
        '''
        self.db = Database(db_path)
        self.db.create()
#         x_ind = Md5Index(self.db.path, 'idx1')
        x_ind = VariableLengthKeyIndex(self.db.path, 'idx1')
        self.db.add_index(x_ind)
    
    def exists(self, key):
        ''' Returns True if the record exists in the DB otherwise returns False. '''
        try:
            self.db.get('idx1', key, with_doc=False)
            return True
        except RecordNotFound:
            return False
        
    def get(self, key):
        ''' Returns the record with the given key, returns None if the key is not in the DB. '''
        try:
            rec = self.db.get('idx1', key, with_doc=True)
            return rec['doc']['value']            
        except RecordNotFound:
            return None
        
    def put(self, key, value):
        ''' Inserts the record in the DB. '''
        try:
            rec = self.db.get('idx1', key, with_doc=True)
            doc = rec['doc']
            doc['idx1'] = key
            doc['value'] = value
            self.db.update(doc)
        except RecordNotFound:
            self.db.insert(dict(idx1=key,value=value))        
        
    def batchWrite(self, records=[]):
        ''' Inserts the provided list of records as an atomic insert. '''
        for record in records:
            self.put(record[0], record[1])        
    
    def delete(self, key):
        ''' Deletes the record with the given key from the DB. Does nothing if key does not exixts in the DB. '''
        try:
            rec = self.db.get('idx1', key, with_doc=True)            
            self.db.delete(rec['doc'])
        except RecordNotFound:
            return
                    
    def getIterator(self, key_from=None, key_to=None):
        ''' Returns the iterator returning the records between the given range of keys. '''
#         DataStoreBase.getIterator(self, key_from, key_to)
        iter_ = self.db.get_many('idx1', key=None, start=key_from, end=key_to, with_doc=True)
#         for rec in iter_:
#             print rec
        return KeyValueIterator(iter_)
        
    def load(self, input_file):
        DataStoreBase.load(self, input_file)
        
    def save(self, output_file):
        DataStoreBase.save(self, output_file)