"""
.. module:: memcached_store
   :platform: Unix
   :synopsis: The module is a Memcached based implementation of the key value datastore.

.. moduleauthor:: Muhammad Ehsan ul Haque <ehsan_haq98@yahoo.com>

"""

from playlistmaker.datastore.kvstore import DataStoreBase
import memcache
import subprocess
import time
import cPickle
from playlistmaker.util.configuration import Config
import string
import codecs
import md5

cfg = Config()
MAX_EXPIRATION_TIME = cfg.getint('memcached', 'max_time', 2592000)
KEY_DUMP_FILE = cfg.get('memcached', 'key_dump_file', '../memcache_keys.dump')

ALL_BYTES = string.maketrans('', '')
CONTROL_CHARACTERS = ALL_BYTES[:33] + ALL_BYTES[127:]

class MemcacheStore(DataStoreBase):
    """
    Key Value data store implementation of :py:class:`DataStoreBase` with Memcached. 
    """
    
    def __init__(self, db_servers, flush=False):
        '''
        Initialize the memcache connection.
        '''
        self.servers = db_servers
        self.db = memcache.Client(db_servers)        
        
        if flush is True:
            self.db.flush_all()
    
    @staticmethod
    def stripControlCharacters(strToStrip):
        """Strip the given string from all the control characters. 
        Control characters are all characters <= 33 and >= 127.
        
        :param strToStrip: string to strip the control characters from.
        :type strToStrip: str.
        :return: the input string stripped from all the control characters.
        """
        return strToStrip.translate(ALL_BYTES, CONTROL_CHARACTERS)
    
    @staticmethod
    def generateHashKey(strToHash):
        """ Returns the :py:mod:`md5` hash of the given string. 
        
        :param strToHash: Input string to generate the hash.
        :type strToHash: str.
        :return: hash as a hex string.
        """
        return md5.new(strToHash).hexdigest()
#         return strToStrip.translate(ALL_BYTES, CONTROL_CHARACTERS)
                                                     
    def exists(self, key):
        """ Check if the key exists in the store. Returns True if the key exists otherwise returns False. 
        
        :param key: key to retrieve value from the store.
        :type key: str.
        :return: True if the key exists in the store otherwise False.
        """                   
        if self.get(key) is None:
            return False
        return True
    
    def get(self, key, returnStale = False):
        """ Returns the value for the given key or return None.
        
        :param key: key to retrieve value from the store.
        :type key: str.
        :param returnStale: Flag for returning stale records, defaults to ``False``.
        :type returnStale: boolean.        
        :return: value stored in the store or None if no match was found.  
        """        
#         dir_rec = self.db.get(self.stripControlCharacters(key))
        dir_rec = self.db.get(self.generateHashKey(key))
        if dir_rec is None or dir_rec['k'] != key:
            return None
        return dir_rec['v']
                        
    def put(self, key, value, time_ = 0):
        """ Put the key, value in the data store, with the given expiration in seconds. 
        
        :param key: key to store with.
        :type key: str.
        :param value: value to store in the store. Value can be any serializable object.
        :type value: object.
        :param time: seconds for which the record will be active. Defaults to 0 max expiration (2592000).
        :type time: int.
        """
        if time_ > MAX_EXPIRATION_TIME or time_ == 0:  # maximum time possible
            time_ = MAX_EXPIRATION_TIME
        dic_rec = {'k': key,'v': value, 'in': int(time.time()), 'ex': time_}
#         self.db.set(self.stripControlCharacters(key), dic_rec, time_)
        self.db.set(self.generateHashKey(key), dic_rec, time_)
#         self.db.set(key, value, time)        
        
    def batchWrite(self, records=[]):
        """ Put the key, value tuple(s) in the list in the data store with max expiration. 
        
        :param records: list of tuples of the form (``key``, ``value``).
        :type records: list.
        """
#         mapping = dict((self.stripControlCharacters(x[0]), \
        mapping = dict((self.generateHashKey(x[0]), \
                        {'k': x[0],'v':x[1], 'in': int(time.time()),'ex': MAX_EXPIRATION_TIME}) \
                       for x in records)
        self.db.set_multi(mapping)        
    
    def delete(self, key):
        """ Delete the key from the data store. Does nothing if the key does not exist. 
        
        :param key: key to delete the record.
        :type key: str.
        """
        if self.get(key) is not None:
            self.db.delete(self.generateHashKey(key))
                    
    def getIterator(self, key_from=None, key_to=None):
        """ Not implemented. Raises ``NotImplementedError``. """
        DataStoreBase.getIterator(self, key_from, key_to)        
        
    def load(self, input_file):
        """ Loads the input data file in the data store. 
        
        :param input_file: input file to read the records from.
        :type input_file: file.
        """
        with open(input_file, "rb") as fIn:        
            try:
                while True:
                    keyValue = cPickle.load(fIn)
                    record = keyValue['value']
                    if record['in'] + record['ex'] < int(time.time()): # expired
                        continue    # skip putting stale record                   
                    self.put(keyValue['key'], record['v'], int(time.time() - record['in'] + record['ex']))
            except EOFError:
                pass

    def save(self, output_file):
        """ Stores the data store in a file to be loaded later. 
        
        :param output_file: output file to store the records.
        :type output_file: file.
        """
        fKey = codecs.open(KEY_DUMP_FILE, 'w')
        returnValue = subprocess.call(['memcdump', '--servers='+','.join(self.servers)], stdout=fKey)        
        if returnValue != 0:
            raise RuntimeError('Failed in exporting keys.')
        fKey.close()
        fKey = codecs.open(KEY_DUMP_FILE, 'r')
        fKeyValue = codecs.open(output_file, 'wb')
        for key in fKey:
            key = key.strip()
            value = self.db.get(key)
            # In file we need to store the actual key, not the stripped/modified key.
            d = {'key': value['k'], 'value': value}
            cPickle.dump(d, fKeyValue, -1)
        fKey.close()
        fKeyValue.close()