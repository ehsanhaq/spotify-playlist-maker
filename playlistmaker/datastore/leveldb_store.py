"""
.. module:: leveldb_store
   :platform: Unix
   :synopsis: The module is a LevelDB based implementation of the key value datastore.

.. moduleauthor:: Muhammad Ehsan ul Haque <ehsan_haq98@yahoo.com>

"""
from playlistmaker.datastore.kvstore import DataStoreBase
import leveldb
import cPickle
import time
from playlistmaker.util.configuration import Config

cfg = Config()
dir_ = cfg.get('leveldb', 'db_path', '../../data/leveldb')
CURRENT_DIR = ('/'.join(__file__.split('/')[:-1]))
DB_DIR = CURRENT_DIR + '/' + dir_

class KeyValueIterator:
    """
    Key Value Iterator to iterate on the leveldb key store. Wraps the underlying iterator provided by the levelDB.    
    """
    def __init__(self, leveldb_iter, return_full_record = False):
        self.iter = leveldb_iter
        self.return_full_record = return_full_record
    
    def __iter__(self):
        return self

    def next(self):
        ''' Returns the next entry in the iterator. '''
        rec = self.iter.next()
        record = cPickle.loads(rec[1])
        if self.return_full_record:
            return (rec[0], record)
        return (rec[0],record['v'])

class LevelDBStore(DataStoreBase):
    """
    Key Value data store implementation of :py:class:`DataStoreBase` with LevelDB. 
    """
    def __init__(self, db_path=None):
        '''
        Opens the DB and creates a new one if not already created.
        '''
        if db_path is None:
            db_path = DB_DIR
        self.db = leveldb.LevelDB(db_path, create_if_missing=True)  # @UndefinedVariable
        
    def exists(self, key):
        """ Check if the key exists in the store. Returns True if the key exists otherwise returns False. 
        
        :param key: key to retrieve value from the store.
        :type key: str.
        :return: True if the key exists in the store otherwise False.
        """
        if self.get(key) is None:
            return False
        return True
        
    def get(self, key, returnStale=False, returnTuple=True):
        """ Returns the value for the given key or return None.
        
        :param key: key to retrieve value from the store.
        :type key: str.
        :param returnStale: Flag for returning stale records, defaults to ``False``.
        :type returnStale: boolean.
        :param returnTuple: Flag for returning a tuple (``True|False``, ``value``), \
        ``True`` and ``False`` indicate if the record is stale. Defaults to ``True``.
        :type returnTuple: boolean.
        :return: value stored in the store or None if no match was found.  
        """        
        try:
            strRecord = self.db.Get(key)
            record = cPickle.loads(strRecord)
            if record['ex'] == 0:   # Not expireable
                record['ex'] = (int(time.time()) + 5)   # Not so good way to prevent expairy.
            if returnStale is True:
                # if it is stale then return with a False flag else return with a True flag.
                if record['in'] + record['ex'] < int(time.time()):
                    if returnTuple:
                        return (False, record['v'])
                    else:
                        return record['v']
                else:
                    if returnTuple:
                        return (True, record['v'])
                    else:
                        return record['v']
            if record['in'] + record['ex'] < int(time.time()):  # expired
                self.delete(key)
                return None
            return record['v']
        except KeyError:
            return None
        
    def put(self, key, value, time_ = 0):
        """ Put the key, value in the data store, with the given expiration in seconds. 
        
        :param key: key to store with.
        :type key: str.
        :param value: value to store in the store. Value can be any serializable object.
        :type value: object.
        :param time: seconds for which the record will be active. Defaults to 0 do not expire.
        :type time: int.
        """
        dic_rec = {'v': value, 'in': int(time.time()), 'ex': time_}
        strRecord = cPickle.dumps(dic_rec)
        self.db.Put(key, strRecord)
        
    def batchWrite(self, records=[]):
        """ Put the key, value tuple(s) in the list in the data store with no expiration. 
        
        :param records: list of tuples of the form (``key``, ``value``).
        :type records: list.
        """
        batch = leveldb.WriteBatch()  # @UndefinedVariable
        current_time = int(time.time())
        for record in records:
            dic_rec = {'v': record[1], 'in': current_time, 'ex': 0}
            strRecord = cPickle.dumps(dic_rec)
            batch.Put(record[0], strRecord)
        self.db.Write(batch)
    
    def delete(self, key):
        """ Delete the key from the data store. Does nothing if the key does not exist. 
        
        :param key: key to delete the record.
        :type key: str.
        """
        return self.db.Delete(key)
                    
    def getIterator(self, key_from=None, key_to=None):
        ''' Returns an iterator to iterate the records from ``key_from`` to ``key_to``.
        
        :param key_from: the lowest bound (inclusive) to start the iterator. Defaults to None which \
        means start from the first record in the store.
        :type key_from: str.
        :param key_to: the upper bound (inclusive) to end the iterator. Defaults to None which \
        means end at the last record in the store.
        :type key_to: str.
        :return: An iterator of type :py:class:`KeyValueIterator`.
        '''
        return KeyValueIterator(self.db.RangeIter(key_from, key_to))        
        
    def load(self, input_file):
        """ Loads the input data file in the data store. 
        
        :param input_file: input file to read the records from.
        :type input_file: file.
        """
        with open(input_file, "rb") as fIn:
            try:
                while True:
                    keyValue = cPickle.load(fIn)
                    record = keyValue['value']
                    if record['in'] + record['ex'] < int(time.time()): # expired
                        continue    # skip putting stale record                   
                    self.put(keyValue['key'], record['v'], int(time.time() - record['in'] + record['ex']))
            except EOFError:
                pass
        
    def save(self, output_file):
        """ Stores the data store in a file to be loaded later. 
        
        :param output_file: output file to store the records.
        :type output_file: file.
        """
        fKeyValue = open(output_file, 'wb')
        for (key,value) in KeyValueIterator(self.db.RangeIter(), True):            
            d = {'key': key, 'value': value}            
            cPickle.dump(d, fKeyValue, -1)
        fKeyValue.close()