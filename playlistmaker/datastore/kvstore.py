"""
.. module:: kvstore
   :platform: Unix
   :synopsis: The module consists of the abstraction needed for implementing key/value datastore.
              The abstraction is meant to implement all kinds of caching/storage requirements
              needed in the whole project.

.. moduleauthor:: Muhammad Ehsan ul Haque <ehsan_haq98@yahoo.com>

"""

import abc

class DataStoreBase(object):
    """
    Abstract class for the key value data store used as a cache for storing key value
    records. The class provides the methods that are needed to be implemented by the
    implementing class(es).    
    """
    __metaclass__ = abc.ABCMeta    
    
    @abc.abstractmethod
    def get(self, key, returnStale=False):
        ''' Returns the value for the given key or return None.
        
        :param key: key to retrieve value from the store.
        :type key: str.
        :param returnStale: Flag for returning stale records, defaults to ``False``.
        :type returnStale: boolean.
        :return: value stored in the store or None if no match was found.  
        '''
        raise NotImplementedError('Method get(key): not implemented.')
    
    @abc.abstractmethod
    def exists(self, key):
        ''' Check if the key exists in the store. Returns True if the key exists otherwise returns False. 
        
        :param key: key to retrieve value from the store.
        :type key: str.
        :return: True if the key exists in the store otherwise False.
        '''
        raise NotImplementedError('Method exists(key): not implemented.')
    
    @abc.abstractmethod
    def put(self, key, value, time):
        ''' Put the key, value in the data store, with the given expiration in seconds. 
        
        :param key: key to store with.
        :type key: str.
        :param value: value to store in the store. Value can be any serializable object.
        :type value: object.
        :param time: seconds for which the record will be active.
        :type time: int.
        '''
        raise NotImplementedError('Method put(key, value): not implemented.')    
    
    @abc.abstractmethod
    def batchWrite(self, records=[]):
        ''' Put the key, value tuple(s) in the list in the data store. 
        
        :param records: list of tuples of the form (``key``, ``value``).
        :type records: list.
        '''
        raise NotImplementedError('Method put(key, value): not implemented.')
    
    @abc.abstractmethod
    def delete(self, key):
        ''' Delete the key from the data store. Does nothing if the key does not exist. 
        
        :param key: key to delete the record.
        :type key: str.
        '''
        raise NotImplementedError('Method delete(key): not implemented.')
    
    @abc.abstractmethod
    def getIterator(self, key_from=None, key_to=None):
        ''' Returns an iterator to iterate the records from ``key_from`` to ``key_to``. This \
        is used only those implementations where the underlying storage stores the records \
        in a sorted order. In othe implementation this call should raise a ``NotImplementedError``.
        
        :param key_from: the lowest bound (inclusive) to start the iterator. Defaults to None which \
        means start from the first record in the store.
        :type key_from: str.
        :param key_to: the upper bound (inclusive) to end the iterator. Defaults to None which \
        means end at the last record in the store.
        :type key_to: str.
        :return: An iterator.        
        '''        
        raise NotImplementedError('Method getIterator(key_from, key_to): not implemented.')
    
    @abc.abstractmethod
    def load(self, input_file):
        ''' Loads the input data file in the data store. 
        
        :param input_file: input file to read the records from.
        :type input_file: file.
        '''
        raise NotImplementedError('Method load(input_file): not implemented.')
    
    @abc.abstractmethod
    def save(self, output_file):
        ''' Stores the data store in a file to be loaded later. 
        
        :param output_file: output file to store the records.
        :type output_file: file.
        '''
        raise NotImplementedError('Method save(output_file): not implemented.')
        