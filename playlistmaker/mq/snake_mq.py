'''
Created on Aug 8, 2013

@author: ehsan
'''

from playlistmaker.mq.mq import QueueEndPoint, MessageQueue
from Queue import Queue, Empty
from snakemq.link import Link
from snakemq.packeter import Packeter
from snakemq.messaging import Messaging
from snakemq.message import Message
import thread
import time
from playlistmaker.util.configuration import Config

cfg = Config()
EP1_ID = cfg.get('snake_mq', 'end_point1_id', 'ep1')
EP2_ID = cfg.get('snake_mq', 'end_point2_id', 'ep2')

class SnakeMQEndPoint(QueueEndPoint):
    '''
    Constructor 
    '''
    def __init__(self, messaging, receiver_ident, endpoint_queue):
        self.messaging = messaging
        self.receiver_ident = receiver_ident        
        self.endpoint_queue = endpoint_queue
    
    def read(self, blocking=True, timeout=None):
#         print 'Reading from: %d' % id(self.endpoint_queue)
        try:
            return self.endpoint_queue.get(blocking, timeout)
        except Empty:
            return None
    
    def write(self, message):
        snake_message = Message(message)
#         print 'Sending message to: ' + self.receiver_ident
        self.messaging.send_message(self.receiver_ident, snake_message)        

def thread_func(link):
    link.loop()
    
class SnakeMQ(MessageQueue):
    '''
    Message Queue implementation with Zero MQ
    '''
    
    def __init__(self, server, frontend_port, backend_port):
        '''
        Constructor
        '''
        self.ep1MessageQueue = Queue()
        self.ep2MessageQueue = Queue()
#         print 'ep1 Queue {0}'.format(id(self.ep1MessageQueue))
#         print 'ep2 Queue {0}'.format(id(self.ep2MessageQueue))
        link1 = Link()
        link1.add_listener(('', frontend_port))
        link1.add_connector((server, backend_port))
        packeter1 = Packeter(link1)
        messaging1 = Messaging(EP1_ID, "", packeter1)
        messaging1.on_message_recv.add(self._ep1_recever)
                
        link2 = Link()
        link2.add_listener(('', backend_port))
        link2.add_connector((server, frontend_port))        
        packeter2 = Packeter(link2)
        messaging2 = Messaging(EP2_ID, "", packeter2)
        messaging2.on_message_recv.add(self._ep2_recever)        
        
        self.ep1 = SnakeMQEndPoint(messaging1, EP2_ID,self.ep1MessageQueue)
        self.ep2 = SnakeMQEndPoint(messaging2, EP1_ID,self.ep2MessageQueue)
        
        try:
            thread.start_new_thread(thread_func, (link1,))
            thread.start_new_thread(thread_func, (link2,))
        except:
            raise
        self.links = (link1, link2)
        time.sleep(2)
    
                    
    def _ep1_recever(self, conn, ident, message):
#         print 'Received in: {0} and putting in {1}'.format(ident, id(self.ep1MessageQueue))
        self.ep1MessageQueue.put_nowait(message.data)        
    
    def _ep2_recever(self, conn, ident, message):
#         print 'Received in: {0} and putting in {1}'.format(ident, id(self.ep2MessageQueue))
        self.ep2MessageQueue.put_nowait(message.data)
    
    def getEndPoint1(self):
        return self.ep1
    
    def getEndPoint2(self):
        return self.ep2
        
    def tearDown(self):
        self.links[0].stop()
        self.links[1].stop()
        time.sleep(1)
        self.links[0].cleanup()        
        self.links[1].cleanup()