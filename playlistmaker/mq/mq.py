'''
Created on Aug 7, 2013

@author: ehsan
'''
import abc

class MessageQueue(object):
    '''
    Abstract class for message queue.
    '''
    
    __metaclass__ = abc.ABCMeta    
    
    @abc.abstractmethod
    def getEndPoint1(self):
        ''' Get the producer of the Message Queue. '''
        raise NotImplementedError('Method getProducer(): not implemented.')
    
    @abc.abstractmethod
    def getEndPoint2(self):
        ''' Get the consumer of the Message Queue. '''
        raise NotImplementedError('Method getConsumer(): not implemented.')
    
    @abc.abstractmethod
    def tearDown(self):
        ''' Teardown the message queue. '''
        raise NotImplementedError('Method tearDown(): not implemented.')

class QueueEndPoint(object):
    ''' 
    Abstract class for the end point of the message queue. The endpoint of the queues are used
    to read and write messages to the queue. 
    '''
    
    __metaclass__ = abc.ABCMeta    
    
    @abc.abstractmethod
    def read(self, blocking, timeout):
        ''' Read message from the endpoint. '''
        raise NotImplementedError('Method read(): not implemented.')
    
    @abc.abstractmethod
    def write(self, message):
        ''' Write message to the endpoint. '''
        raise NotImplementedError('Method write(): not implemented.')