'''
Created on Aug 6, 2013

@author: ehsan
'''

import zmq
from playlistmaker.mq.mq import MessageQueue, QueueEndPoint
from zmq.devices.basedevice import ProcessDevice

class ZeroMQEndPoint(QueueEndPoint):
    '''
    Constructor 
    '''
    def __init__(self, socket):
        self.socket = socket
    
    def read(self, blocking=True, timeout=None):
        return self.socket.recv()
    
    def write(self, message):
        self.socket.send(message)
    
class ZeroMQ(MessageQueue):
    '''
    Message Queue implementation with Zero MQ
    '''
    
    def __init__(self, server, frontend_port, backend_port):
        '''
        Constructor
        '''
        self.server = server
        self.frontend_port = frontend_port
        self.backend_port = backend_port
        queuedevice = ProcessDevice(zmq.QUEUE, zmq.XREP, zmq.XREQ)  
        queuedevice.bind_in("tcp://{0}:{1}".format(self.server, frontend_port))
        queuedevice.bind_out("tcp://{0}:{1}".format(self.server, backend_port))
        queuedevice.setsockopt_in(zmq.HWM, 1)  
        queuedevice.setsockopt_out(zmq.HWM, 1)      
        queuedevice.start()
        
        context = zmq.Context()
        socket1 = context.socket(zmq.REP)
        socket1.connect("tcp://{0}:{1}".format(self.server, backend_port))
        self.ep1 = ZeroMQEndPoint(socket1)
        
        socket2 = context.socket(zmq.REP)
        socket2.connect("tcp://{0}:{1}".format(self.server, frontend_port))
        self.ep2 = ZeroMQEndPoint(socket2)
        
    def getEndPoint1(self):
        return self.ep1
    
    def getEndPoint2(self):
        return self.ep2
        
    def tearDown(self):
        pass