'''
Created on Aug 10, 2013

@author: ehsan
'''

from flask import Flask, redirect
from flask.globals import request
from playlistmaker.web.request_handler import RequestHandler
from Queue import Queue
from flask.helpers import url_for

app = Flask(__name__)

@app.route('/')
def index():
    return redirect(url_for('make_playlist'))
#     time.sleep(10)
#     return "Hello World from Flask. Current time is {0}\n".format(time.asctime())

@app.route('/make_playlist')
def make_playlist():
    inputText=request.args.get('input')
#     print inputText
    resultQueue = Queue()
    handler = RequestHandler(inputText, resultQueue)
    r = []
    for result in handler.process():
        r.append(result)
    return str(r)
        
if __name__ == '__main__':
    app.run(host='0.0.0.0', port = 9000)
#     app.run(host='0.0.0.0', port=9000, server='gunicorn', workers=10)