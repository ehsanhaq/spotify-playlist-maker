'''
Created on Aug 11, 2013

@author: ehsan
'''
import logging

from gevent import monkey; monkey.patch_all()
from bottle import run, route, request, redirect, error, static_file
from playlistmaker.web.request_handler import RequestHandler
from gevent.queue import Queue
from playlistmaker.util.configuration import Config

logging.basicConfig(filename='../../log/playlist_maker.log', level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

cfg = Config()
HOST = cfg.get('bottle_server', 'host', '0.0.0.0')
PORT = cfg.getint('bottle_server', 'port', 9001)
SERVER = cfg.get('bottle_server', 'server', 'gevent')
STATICROOT = cfg.get('bottle_server', 'static', '../../docs/build/html/')

@route('/')
def main_url():
    redirect("/make_playlist")

@route('/docs')
def docs():
    return static_file('index.html', root=STATICROOT)

@route('/<filename:path>') 
def static(filename): 
    return static_file(filename, root=STATICROOT)

@route('/make_playlist')
def make_playlist():
    inputText=request.query.input
    resultQueue = Queue()
    handler = RequestHandler(inputText, resultQueue)
    for result in handler.process():
        yield result

@error(404)
def error404(error):
    return 'Nothing here, sorry'

run(host=HOST, port=PORT, server=SERVER, debug=True)
