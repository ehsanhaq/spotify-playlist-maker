'''
Created on Aug 11, 2013

@author: ehsan
'''
import threading
import time
import Queue
from playlistmaker.datastore.leveldb_store import LevelDBStore
from playlistmaker.spotifyapi.metastore import SpotifyMetaStoreAPI
from playlistmaker.playlist_maker import TextToPlayList
import logging

# logger = logging.getLogger(__name__)

class RequestHandler(object):
    '''
    Class that handles and process the REST API request, to generate the playlist.
    '''        
    db = LevelDBStore()
    api = SpotifyMetaStoreAPI(db)
    maker = TextToPlayList(db, api)    
    def __init__(self, inputText, resultQueue):
        '''
        Constructor
        '''
        self.inputText = inputText
        self.resultQueue = resultQueue
    
    def process(self):
        timeout = 5
        stopThread= threading.Event()
        queue = Queue.Queue()
        result = None
        logging.info('Processing Request For: %s', self.inputText)
        t1 = threading.Thread(target=self.maker.createPlayList, \
                              args=(self.inputText, ' ', queue, stopThread))
        t1.start()
        initTime = time.time()
        while True:
            currentTime = time.time()
            try:
                newTimeout = timeout - (currentTime - initTime)
                result = queue.get(block = True, timeout = newTimeout if newTimeout > 0 else 0)
                logging.debug('Got Result: %s', str(result))
                self.resultQueue.put(result)
                queue.task_done()
                yield ''.join(['<p> ', str(result), '<br/>', 'Improving Search...' ,' </p>\n'])                
            except Queue.Empty:
                stopThread.set()
                break
            if result['score'] >= 0.99 or currentTime - initTime >= timeout:
                stopThread.set()
                break
        t1.join()
        while not queue.empty():
            result = queue.get_nowait()
            self.resultQueue.put(result)
            queue.task_done()
            yield ''.join(['<p> ', str(result), '<br/>', 'Improving Search...' ,' </p>\n'])
        logging.info('Processing Finished For: %s', self.inputText)
        yield ''.join(['<p> ', 'Search Finished', '</p>\n'])
        yield ''.join(['<p> ', 'Searching again may improve results!!', '</p>\n'])
        self.resultQueue.put(StopIteration)
        