"""
.. module:: parser
   :platform: Unix
   :synopsis: The module consists of the abstraction needed for implementing parser for
              the spotify metadata api.              

.. moduleauthor:: Muhammad Ehsan ul Haque <ehsan_haq98@yahoo.com>

"""

import abc
import inspect
from playlistmaker.util.configuration import Config

cfg = Config()
HEADER = cfg.get('parser_fields', 'header', 'header')
TRACKS = cfg.get('parser_fields', 'tracks', 'tracks')
H_QUERY = cfg.get('parser_header_fields', 'query', 'query')
H_LIMIT = cfg.get('parser_header_fields', 'limit', 'limit')
H_OFFSET = cfg.get('parser_header_fields', 'offset', 'offset')
H_PAGE = cfg.get('parser_header_fields', 'page', 'page')
H_TOTAL = cfg.get('parser_header_fields', 'total', 'total')
T_HREF = cfg.get('parser_track_fields', 'href', 'href')
T_NAME = cfg.get('parser_track_fields', 'name', 'name')
T_LENGTH = cfg.get('parser_track_fields', 'length', 'length')
T_POPULARITY = cfg.get('parser_track_fields', 'popularity', 'popularity')

class TrackListParser(object):
    """
    Abstract class for parsing the search results from the spotify metastore api. 
    """
    
    __metaclass__ = abc.ABCMeta
        
    @abc.abstractmethod
    def parse(self, data):
        """ Parses the data into a dictionary. """
        raise NotImplementedError('Method parse(data): not implemented.')
    
    def extractAllFields(self):
        """ Calls all the methods in the instance with a name _extract*. This method is just
        a helper to prevent all the individul extract methods used during the parsing.
        """
        parsed = {}
        for method in inspect.getmembers(self, predicate=inspect.ismethod):
            if method[0].startswith('_extract'):
                tuple_ = method[1]()
                parsed[tuple_[0]] = tuple_[1]
        return parsed
    
    @abc.abstractmethod
    def _extractHeader(self):
        """ Extract the basic header information from the query result of the spotify metastore api. """
        raise NotImplementedError('Method _extractHeader(): not implemented.')
    
    @abc.abstractmethod
    def _extractTracks(self):
        """ Extract the track information from the query result of the spotify metastore api. """
        raise NotImplementedError('Method _extractTracks(): not implemented.')
    
    