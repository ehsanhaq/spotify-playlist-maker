"""
.. module:: metastore
   :platform: Unix
   :synopsis: The module deals with all the details of query making to the spotify metadata api
              and fetching storing the queries in the cache.

.. moduleauthor:: Muhammad Ehsan ul Haque <ehsan_haq98@yahoo.com>

"""

import urllib2
from datetime import datetime
from dateutil import tz
from playlistmaker.util.configuration import Config
from playlistmaker.util.ratelimit import RateLimiter

cfg = Config()
JSON_URL_PREFIX = cfg.get('metastore', 'json_url_prefix', 'http://ws.spotify.com/search/1/track.json?')
XML_URL_PREFIX = cfg.get('metastore', 'xml_url_prefix', 'http://ws.spotify.com/search/1/track?')

soptifyAPIRateLimiter = RateLimiter('spotify_api', 8)
API_TIME_FORMAT = cfg.get('metastore', 'api_time_format', '%a, %d %b %Y %H:%M:%S %Z')
expochTime = 'Thu, 01 Jan 1970 00:00:00 GMT'

class SpotifyMetaStoreAPI(object):
    '''
    class for using spotify metastore API. The class also manages all the querying and storing of
    search results from Spotify Metadata API into local cache.
    '''

    def __init__(self, db):
        """
        Constructor requires a cache ``datastore.kvstore`` instance. The datastore instance is used
        to search and store data in the cache.
        
        :param db: Cache datastore instance.
        :type db: instance of ``datastore.kvstore`` 
        """
        self.db = db
    
    @staticmethod
    def _extractHeadersFromList(headerList):
        """ Utility function that converts the HTTP header in the form list of string like
        ['header1: header value', 'header2: header value', ...] into dictionary of key value
        items like {'header1': 'header value', 'header2': 'header value', ...}
        
        :param headerList: input list of strings, each string is of the form 'header: value'.
        :type headerList: list
        :return: dictionary of header and their values.
        """
        headers = dict(zip(\
                            map(lambda x: x.split(':',1)[0].strip(), headerList), \
                            map(lambda x: x.split(':',1)[1].strip(' \r\n'), headerList)))
        return headers
    
    @staticmethod
    def _extractHeaders(response):        
        """ Extracts the header from the HTTP response and convert it into a dictionary using \
        :py:func:`SpotifyMetaStoreAPI._extractHeadersFromList`
        
        :param response: HTTP respoonse retrieved from Spotify Metadata API.
        :type response: HTTPResponse
        :return: dictionary of header and their values.         
        """
        headerList = response.headers.headers
        return SpotifyMetaStoreAPI._extractHeadersFromList(headerList)     
    
    @staticmethod
    def _changeTimeZone(dateTime, toZone = tz.tzlocal()):
        """ Utility function to change the timezone of a given datetime.
        
        :param dateTime: dateTime string of the form YYYY-MM-dd hh:mm:ss zzz
        :type dateTime: str.
        :param toZone: The time zone in which to convert. Defaults to local timezone.
        :type toZone: tzinfo
        :return: string representation of ``dateTime`` with time zone converted to ``toZone``.
        """
        fromZoneStr = dateTime.split()[-1]
        fromZone = tz.gettz(fromZoneStr)
        dtNaive = datetime.strptime(dateTime, API_TIME_FORMAT)
        dtTimeZoned = dtNaive.replace(tzinfo=fromZone)
        converted = dtTimeZoned.astimezone(toZone)
        return converted        
    
    @staticmethod
    def _packResultIntoDictionary(response):
        """ Utility function that converts the HTTP response into a dictionary with url, headers \
        data and format.
        
        :param response: HTTP response of the search track.
        :type respons: HTTPResponse
        :return: a dictionary with parsed key/value items from the HTTP response.
        """
        url = response.geturl()
        responseFormat = 'json' if url.startswith(JSON_URL_PREFIX) else 'xml'
        headers = SpotifyMetaStoreAPI._extractHeaders(response)
        dataStr = response.read()
        return {'url': url, 'headers':headers, 'data': dataStr, 'format': responseFormat}
    
    def _apiSearchIfModified(self, oldRecord):
        """ Method for sending a request for the track with an ``If-Modified-Since`` flag.
        If the request with If-Modified_since flag returns an HTTP response code 304 (Not modified),
        then the ``oldRecord`` is returned otherwise if the response is 200 then the record with
        new data is cached and returned.
        
        :param oldRecord: An old cached record which has expired.
        :type oldRecord: dictionary.        
        :return: A dictionary of old cached record or a new dictionary with new response.  
        """
        url = oldRecord['url']
        lastModified = oldRecord['headers']['Last-Modified']
        req = urllib2.Request(url=url, headers={'If-Modified-Since': lastModified})
        try:
            response = urllib2.urlopen(req)
        except urllib2.HTTPError as e:
            if e.code == 304:   # Not Modified
                return oldRecord
            else:   # Some other exception, should raise it.
                raise e
        return self._packResultIntoDictionary(response)
    
    def _searchFromCache(self, key):
        """ Method to search from the cache storage. This method also calls the ``_apiSearchIfModified``
        if the cache entry is found but stale.
        
        :param key: the key to search in the cache storage.
        :type key: str.
        :return: returns a cached record if entry is not stale, or a record returned from \
        ``_apiSearchIfModified`` if cached entry is stale or None in case no entry found in the cache.
        """        
        # When searched with returnStale-True, then returns a tuple (boolean, record). True in 
        # the first element of tuple means record is not stale.
        record = self.db.get(key, returnStale = True)
        if record is None:
            return None
        if isinstance(record, (list, tuple)):
            if record[0] is False:
                record = self._apiSearchIfModified(record[1])
            else:
                record = record[1]
        return record
    
    def _apiSearchForNewRecord(self, searchText, pageStr, responseFormat):
        """Method to search records that were not found in the cache entries and now needed to be requested
        from Spotify Metadata API.
        
        :param searchText: key to search with.
        :type searchText: str.
        :param pageStr: Page number to search from Metadata API.
        :type pageStr: str.
        :param responseFormat: JSON or XML to use to request and get response to.
        :type responseFormat: str
        :return: A dictionary containing various items extracted from HTTPResponse as returned by \
        ``_packResultIntoDictionary``
        """
        if responseFormat.lower() == 'json':
            url = ''.join([JSON_URL_PREFIX, searchText, pageStr])
        else:   # default xml
            url = XML_URL_PREFIX + searchText + pageStr
        response = urllib2.urlopen(url)
        return self._packResultIntoDictionary(response)
    
    def _cacheSearchResult(self, key, searchResult):
        """Cache the response from Metadata API.
        
        :param key: key to cache with.
        :type key: str.
        :param searchResult: Dictionary containing the extracted elements from HTTPResponse.
        :type searchResult: dictionary.        
        """
        expirationTime = searchResult['headers']['Expires']
        secondsLeft = int((self._changeTimeZone(expirationTime) - datetime.now(tz.tzlocal())).total_seconds())
        self.db.put(key, searchResult, secondsLeft)

    def search(self, searchText, page=None, responseFormat='xml'):
        """Main search method that is / should be called from outside the class to search for tracks. This method
        encapsulates the logic of first searching from cache and searching from MetadataAPI if search from cache
        fails. This method also stores the Metadata API response in cache.
         
        :param searchText: search text to search for tracks.
        :type searchText: str.
        :param page: Page number, defaults to None.
        :type page: str.
        :param responseFormat: XML or JSON.
        :type responseFormat: str.
        :return: A dictionary containing various items extracted from HTTPResponse as returned by \
        ``_packResultIntoDictionary``
        """
        searchText = urllib2.quote(searchText)
        pageStr = '' if page is None else '&page={0}'.format(page)
        key = searchText.lower() + ('1' if page is None else str(page)) + responseFormat                    
        record = self._searchFromCache(key)
        if record is None:
            soptifyAPIRateLimiter.do()
            record = self._apiSearchForNewRecord('q=' + searchText, pageStr, responseFormat)
            self._cacheSearchResult(key, record)
        return record