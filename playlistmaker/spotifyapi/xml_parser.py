"""
.. module:: xml_parser
   :platform: Unix
   :synopsis: The module is a XML version parser implementation of the spotify metadata api.              

.. moduleauthor:: Muhammad Ehsan ul Haque <ehsan_haq98@yahoo.com>

"""

from playlistmaker.spotifyapi.parser import TrackListParser
import parser
import xml.etree.ElementTree as ET

class XMLTrackListParser(TrackListParser):
    """
    XML implementation of the spotify track list parser.
    """

    def __init__(self, data, namespaces):
        """
        Constructor that takes the ``data`` returned from the spotify metadata xml api.
        The constructor also calls the ``parse`` method as well which parses the whole message
        and puts the parsed result as a dictionary in a local variable ``parsed`` of the instance.
        
        :param data: the xml message returned from the spotify xml search track api.
        :type data: XML string.
        :param namespace: namespace(s) used during the XML parsing of elements.
        :type namespace: dictionary 
        """
        self.namespaces = namespaces
        self.parse(data)
            
    def parse(self, data, namespaces=None):
        """ Parses the data into a dictionary.
        
        :param data: the xml message returned from the spotify xml search track api.
        :type data: XML string.
        :param namespace: namespace(s) used during the XML parsing of elements.
        :type namespace: dictionary
        :return: a parsed result as dictionary containing selected information(s) from the input xml. 
        """
        if namespaces is not None:
            self.namespaces = namespaces
        self.root = ET.fromstring(data)
        self.parsed = self.extractAllFields()
        return self.parsed
        
    def _extractHeader(self):
        """
        Returns the basic information of tracks returned from the spotify xml metadata search track api
        as a dictionary.
        
        :return: dictionary
        """
        header = {}
        queryAttrib = self.root.find('opensearch:Query', self.namespaces).attrib
        header[parser.H_QUERY] = queryAttrib['searchTerms']
        header[parser.H_LIMIT] = int(self.root.find('opensearch:itemsPerPage', self.namespaces).text)
        header[parser.H_OFFSET] = int(self.root.find('opensearch:startIndex', self.namespaces).text)
        header[parser.H_TOTAL] = int(self.root.find('opensearch:totalResults', self.namespaces).text)
        header[parser.H_PAGE] = int(header['offset'])/int(header['limit'])
        return (parser.HEADER, header)
        
    def _extractTracks(self):
        """
        Returns the basic information of tracks returned from the spotify xml metadata search track api
        as a dictionary.
        
        :return: dictionary
        """
        uniqueTracks = {}
        trackElements = self.root.findall('xmlns:track', self.namespaces)
        for trackElement in trackElements:
            href = trackElement.attrib['href']
            name = trackElement.find('xmlns:name', self.namespaces).text
            lcaseName = name.lower()
            length = float(trackElement.find('xmlns:length', self.namespaces).text)
            popularity = float(trackElement.find('xmlns:popularity', self.namespaces).text)            
            if (lcaseName not in uniqueTracks) or (uniqueTracks[lcaseName]['popularity'] < popularity):
                uniqueTracks[lcaseName] = {parser.T_HREF: href, parser.T_NAME: name, \
                                      parser.T_LENGTH: length, parser.T_POPULARITY: popularity}
        return (parser.TRACKS, uniqueTracks)