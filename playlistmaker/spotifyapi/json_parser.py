"""
.. module:: json_parser
   :platform: Unix
   :synopsis: The module is a JSON version parser implementation of the spotify metadata api.              

.. moduleauthor:: Muhammad Ehsan ul Haque <ehsan_haq98@yahoo.com>

"""

import json
import parser
from playlistmaker.spotifyapi.parser import TrackListParser

class JSONTrackListParser(TrackListParser):
    """
    JSON implementation of the spotify track list parser.
    """

    def __init__(self, data):
        """
        Constructor that takes the ``data`` returned from the spotify metadata json api.
        The constructor also calls the ``parse`` method as well which parses the whole message
        and puts the parsed result as a dictionary in a local variable ``parsed`` of the instance.
        
        :param data: the json message returned from the spotify json search track api.
        :type data: JSON 
        """
        self.parse(data)
    
    def parse(self, data):
        """ Parses the data into a dictionary.
        
        :param data: the json message returned from the spotify json search track api.
        :type data: JSON
        :return: a parsed result as dictionary containing selected information(s) from the input json. 
        """
        self.jsonObj = json.loads(data)
        self.parsed = {}
        self.parsed = self.extractAllFields()
        return self.parsed
    
    def _extractHeader(self):
        """
        Returns the basic information of tracks returned from the spotify json metadata search track api
        as a dictionary.        
        
        :return: dictionary
        """
        header = {}        
        header[parser.H_QUERY] = self.jsonObj['info']['query']
        header[parser.H_LIMIT] = int(self.jsonObj['info']['limit'])
        header[parser.H_OFFSET] = int(self.jsonObj['info']['offset'])
        header[parser.H_TOTAL] = int(self.jsonObj['info']['num_results'])
        header[parser.H_PAGE] = int(self.jsonObj['info']['page'])
        return (parser.HEADER, header)
    
    def _extractTracks(self):
        """
        Returns the basic information of tracks returned from the spotify json metadata search track api
        as a dictionary.
        
        :return: dictionary
        """
        uniqueTracks = {}
        tracks = self.jsonObj['tracks']
        for track in tracks:
            href = track['href']
            name = track['name']
            lcaseName = name.lower()
            length = float(track['length'])
            popularity = float(track['popularity'])            
            if (lcaseName not in uniqueTracks) or (uniqueTracks[lcaseName]['popularity'] < popularity):
                uniqueTracks[lcaseName] = {parser.T_HREF: href, parser.T_NAME: name, \
                                      parser.T_LENGTH: length, parser.T_POPULARITY: popularity}
        return (parser.TRACKS, uniqueTracks)