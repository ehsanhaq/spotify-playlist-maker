'''
Created on Aug 16, 2013

@author: ehsan
'''

from ConfigParser import NoOptionError, NoSectionError
import ConfigParser

CURRENT_DIR = ('/'.join(__file__.split('/')[:-1]))

class Config(object):
    NO_DEFAULT = object()    
    
    config = ConfigParser.RawConfigParser()
    config.read(CURRENT_DIR + '/../../resources/playlist_maker.cfg')
    __shared_state = {'config':config}
    def __init__(self):        
        self.__dict__ = self.__shared_state        
    
    def _get_with_default(self, method, section, option, default, expected_type=None):
        """ 
        Gets the value of the section/option using method. Returns default if value
        is not found. Raises an exception if the default value is not None and doesn't match
        the expected_type.
        """
        try:
            return method(section, option)
        except (NoOptionError, NoSectionError):
            if default is Config.NO_DEFAULT:
                raise
            if expected_type is not None and default is not None and \
               not isinstance(default, expected_type):
                raise
            return default
        
    def get(self, section, option, default=NO_DEFAULT):
        return self._get_with_default(self.config.get, section, option, default)

    def getboolean(self, section, option, default=NO_DEFAULT):
        return self._get_with_default(self.config.getboolean, section, option, default, bool)

    def getint(self, section, option, default=NO_DEFAULT):
        return self._get_with_default(self.config.getint, section, option, default, int)

    def getfloat(self, section, option, default=NO_DEFAULT):
        return self._get_with_default(self.config.getfloat, section, option, default, float)