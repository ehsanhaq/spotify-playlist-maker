"""
.. module:: ratelimit
   :platform: Unix
   :synopsis: The module provides a rate limiting mechanism.

.. moduleauthor:: Muhammad Ehsan ul Haque <ehsan_haq98@yahoo.com>

"""
import time
from threading import Lock
from playlistmaker.util.configuration import Config

cfg = Config()
SLEEP_PERIOD = cfg.getfloat('rate_limiter', 'blocking_period', 0.25)    
    
class RateLimiter(object):
    """ A Generic class to rate limit any kind of activity.
    Based on the Borg pattern to implement a singleton based on id parameter.
    """
    __instances = {}
    def __init__(self, id_, rate):
        """ Constructor for creating a RateLimit instance or returning an already created one.
        
        :param id_: An identifier for the instance, which is used to prevent recreation of \
        instance with same identifier.
        :type id_: object.
        :param rate: rate (number of calls per second) to limit.
        :type rate: int.  
        """
        if id_ not in self.__instances:
            self.__instances[id_] = {'rate':rate, 'lock':Lock()}
        shared_state = self.__instances[id_]
        self.__dict__ = shared_state
    
    def do(self):
        """ Method to apply the rate limit control. The call tries to acquire a lock created
        during the instantiation of the object, to synchronize the calls and make the rate limit possible.
        The call sleeps for a small period of time (configurable) if the number of calls exceed the
        threshold.
        """
        self.lock.acquire()
        curr_time = int(time.time())        
        if 'last_time' not in self.__dict__:
            self.last_time = curr_time
            self.count = 0        
        if self.last_time != curr_time:
            self.count = 0            
        if self.count >= self.rate:
            time.sleep(SLEEP_PERIOD)
            self.lock.release()
            self.do()
            return
        self.last_time = curr_time
        self.count += 1
        self.lock.release()