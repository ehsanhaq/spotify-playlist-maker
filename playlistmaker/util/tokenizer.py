'''
Created on Jul 27, 2013

@author: ehsan
'''

from collections import deque
from sets import Set
import sys
import math
            
class TextTokenizer(object):
    '''
    Class for tokenizing text into possible key words for searching the track.
    '''

    def __init__(self, text, delimeter=' ', diatomics=Set([]), minWords=0, maxWords=sys.maxint):
        self.delimeter = delimeter
        self.diatmoics = diatomics
        self.minWords = minWords
        self.maxWords = maxWords
        self.tokens = deque(text.split(delimeter))
    
    def next_token(self):
        ''' Returns the next token. '''
        token = self.tokens.popleft()
        words = 1
        while (self.tokens) and (words < self.maxWords) and \
            ((words < self.minWords and self.tokens) or \
            (token.lower() in [x.lower() for x in self.diatmoics])): # Case insensitive matching
            words += 1
            token = token + self.delimeter + self.tokens.popleft()
        return token
        
    def has_more_tokens(self):
        ''' Returns True if there are more tokens, else returns False. '''
        return len(self.tokens) > 0
    
    def add_diatomic(self, diatomic):
        self.diatmoics.add(diatomic)
    
    @staticmethod
    def generate_tokens(text, delimeter=' ', diatomics=Set([])):
        ''' Tokenize the text and return the list of tokens. '''
        tokenizer = TextTokenizer(text, delimeter, diatomics)
        tokens = []
        while tokenizer.has_more_tokens():
            tokens.append(tokenizer.next_token())
        return tokens            

class GenerationTokenizer(object):
    """
    Class for tokenizing text into possible key words for searching the track.
    Different number of tokens are generated in different generations. Generation is an indication
    for the number of splits of the original text. In generation 1 the input gets split into two parts.
    In generation 2 input gets split into three parts and son on. In addition to generation, in each
    generation a sequence of possible split can be made which is handled by another parameter ``overlap``.
    The class works like an iterator to iterate on possible combinations of the given text broken into
    smaller parts, ordered from smaller number of splits to high number of splits.     
    """
    def __init__(self, text, generation, delimeter=' ', overlap=0):
        """ Create and initialize a tokenizer instance.
        
        :param text: input text to split.
        :type text: str.
        :param generation: starting generation. 
        :type generation: int
        :param delimeter: delimeter to split words from the input text.
        :type delimeter: str.
        :param overlap: ratio to overlap adjacent words in splits.
        :param overlap: double
        """
        if generation < 1:
            raise ValueError('generation should be > 0')
        self.delimeter = delimeter
        self.noPartitions = generation + 1
        self.tokenListsOrig = [[] for _ in range(generation+1)] # empty lists, not to be modified
        self.tokenLists = [[] for _ in range(generation+1)] # empty lists, that is modified
        tokens = text.split(delimeter)
        startIndex = 0
        splitSize = len(tokens)/(self.noPartitions)
        if splitSize == 0:
            splitSize = len(tokens)   # setting to maximum words/tokens.
        i = 0        
        for list_ in self.tokenLists:
            listOrig = self.tokenListsOrig[i]            
            for token in tokens[startIndex:startIndex+splitSize]:
                list_.append(token)
                listOrig.append(token)
            startIndex += splitSize
            i += 1
        # adding the last tokens remained after equal split(s) in the last list.
        self.tokenLists[-1].extend(tokens[startIndex:])
        self.tokenListsOrig[-1].extend(tokens[startIndex:])        
        self.currentPair = 0
        self.overlapTokens = int(math.ceil(splitSize*overlap))        
        self.currentOverlap = 0
        self._shiftFromLeftToRight(self.tokenLists[self.currentPair], \
                                   self.tokenLists[self.currentPair + 1], self.overlapTokens)
    
    def has_more_shuffles(self):
        """ Returns True if there are more shuffles/splits possible, else returns False. """
        if self.currentPair >= (self.noPartitions-1):
            return False
        return True
#         if self.currentOverlap == (self.overlapTokens*2) and self.currentPair == (self.noPartitions-2):
#             return False
#         
#         if (self.currentPair < (self.noPartitions - 1)):
#             return True
#         return False
    
    def _reset_tokenList(self):
        """ Method to reset the states once all the possible splits in a generation are completed. """
        if self.currentPair != 0: # Already reset, don't reset.
            self.tokenLists[self.currentPair-1] = []
            self.tokenLists[self.currentPair-1].extend(self.tokenListsOrig[self.currentPair-1])            
            self.tokenLists[self.currentPair] = []
            self.tokenLists[self.currentPair].extend(self.tokenListsOrig[self.currentPair])            
            self._shiftFromLeftToRight(self.tokenLists[self.currentPair], \
                                       self.tokenLists[self.currentPair + 1], self.overlapTokens)
            return True
        return False
    
    def next_shuffle(self):
        """ Returns the last shuffled pair or raises StopIteration when there are no more shuffles to perform.
        In each generation two adjacent splits are altered to produce a possible split. The method only returns
        the pair that has been shuffled last.
        
        :return: A pair of input split that was shuffled to produce a new combination. \
        Returns StopIteration if no new combination is possible.
        """
        if not self.has_more_shuffles():
            raise StopIteration('no more shuffle to make.')
        if self.currentOverlap == -1:
            self.currentOverlap += 1
            if self._reset_tokenList():
                return self.currentPair
            
        self.currentOverlap += 1
        self._shiftFromRightToLeft(self.tokenLists[self.currentPair], self.tokenLists[self.currentPair+1], 1)
        lastShuffledPair = self.currentPair
        if self.currentOverlap == (self.overlapTokens*2): # All shifts in current pair are done
            # Copying the Orig list into the working lists.
            self.currentPair += 1
            self.currentOverlap = -1
            if (self.currentPair+1) == self.noPartitions: # safe guard check
                return lastShuffledPair                        
        return lastShuffledPair
    
    @staticmethod
    def _shiftFromLeftToRight(leftList, rightList, n):
        """Shift ``n`` items from ``leftList`` to ``rightList``.
        
        :param leftList: List of words from which to remove items from tail.
        :type leftList: list.
        :param rightList: List of words to prepend to.
        :type rightList: list.
        :param n: number of items to shift.
        :type n: int.        
        """
        if len(leftList) < n:
            raise ValueError('items to shift is more than the items in the leftList.')
        for _ in range(n):
            rightList.insert(0, leftList.pop())
    
    @staticmethod
    def _shiftFromRightToLeft(leftList, rightList, n):
        """Shift ``n`` items from ``rightList`` to ``lefttList``.
        
        :param leftList: List of words to which to append..
        :type leftList: list.
        :param rightList: List of words from which to remove items from head.
        :type rightList: list.
        :param n: number of items to shift.
        :type n: int.
        """
        '''
        Shift n items from leftList to rightList.
        '''
        if len(rightList) < n:
            raise ValueError('items to shift is more than the items in the rightList.')
        for _ in range(n):
            leftList.append(rightList.pop(0))            
        