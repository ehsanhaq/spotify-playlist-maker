"""
.. module:: kvstore
   :platform: Unix
   :synopsis: The module consists of the abstraction needed for implementing key/value datastore.
              The abstraction is meant to implement all kinds of caching/storage requirements
              needed in the whole project.            

.. moduleauthor:: Muhammad Ehsan ul Haque <ehsan_haq98@yahoo.com>

"""

from pydoc import deque
from playlistmaker.spotifyapi.xml_parser import XMLTrackListParser
import ast
import difflib
from difflib import SequenceMatcher
from playlistmaker.util.configuration import Config
from playlistmaker.util.tokenizer import GenerationTokenizer

dbPrefix = {'DIATOMIC':'$01$'}
cfg = Config()
MINWORDS = cfg.getint('tokenizer', 'min_words', 2)
MAXWORDS = cfg.getint('tokenizer', 'max_words', 4)
NAMESPACES = ast.literal_eval(cfg.get('parser_xml', 'namespaces'))

class TextToPlayList(object):
    '''
    Classs is responsible for creating spotify playlist from the plain text of word(s).
    '''
    def __init__(self, tokenDB, api):
        '''
        Constructor
        '''        
        self.db = tokenDB
        self.metaAPI = api        
    
    def _getDiatomicFor(self, token):
        key = dbPrefix['DIATOMIC'] + token.lower()
        diatomics = self.db.get(key)
        if diatomics is None:
            return []
        return diatomics
    
    def _addDiatomic(self, token, diatomic):
        key = dbPrefix['DIATOMIC'] + token.lower()
        diatomics = self.db.get(key, returnStale=True, returnTuple=False)
        if isinstance(diatomics, list):           
            diatomics.append(diatomic)
        else:
            diatomics = [diatomic]
        self.db.put(key, diatomics)
        
    def _getDiatmoics(self, text, delimeter):
        tokens = deque(text.split(delimeter))
        combinedDiatomics = []
        for token in tokens:
            # get diatomic that starts with token.
            diatomics = self._getDiatomicFor(token)
            combinedDiatomics.extend(diatomics)
        return combinedDiatomics
    
    @classmethod
    def _filterAndScoreSearchResults(cls, result, topN = 1):
        """
        Filter the top N match from all matching results and return the result with the best match and best score.
        This method takes the result from the search api and filters the top ``topN`` results.
        The search result from the search api is of the form {'header':{'query': ``'query text to search'``, ...}, \
        'tracks':{'track 1': {...}, 'track 2': {...}, ...}. From the list of track (keys in the tracks dict) \
        :py:func:`difflib.get_close_match` is called to find closest ``topN`` matches for ``'query text to search'``.\
        The result is returned as a dictionary.
        
        :param result: The result of the search api after being parsed with :py:class:`XMLTrackListParser`. \
        The format of the dictionary as explained in the description. 
        :type result: dictionary.
        :param topN: Number of close matches to return defaults to 1.
        :type topN: int.
        :return: Filtered search result in the form {'header':{}, 'tracks':{'top match track':{'score': x.xx, ...}}, \
        ..., 'topmatch': 'top match track'} 
        """
        # Sainaty check
        if 'header' not in result or 'tracks' not in result or 'query' not in result['header']:
            return None 
        queryText = result['header']['query'].lower()
        filtered = {'header': result['header'], 'tracks':{}}
        trackList = [trackKey for trackKey in result['tracks'].iterkeys()]
        closeMatches = difflib.get_close_matches(queryText, trackList, topN)
        if closeMatches:
            filtered['topmatch'] = closeMatches[0]
        for trackKey in closeMatches:            
            filtered['tracks'][trackKey] = result['tracks'][trackKey]
            filtered['tracks'][trackKey]['score'] = round(SequenceMatcher(None, queryText, trackKey).ratio(), 2)
        return filtered
        
    def _getTopScoredResult(self, delimeter, tokens):
        token = delimeter.join(tokens)
        searchResult = self.metaAPI.search(token)
        resultParser = XMLTrackListParser(searchResult['data'], NAMESPACES)
        filtered = self._filterAndScoreSearchResults(resultParser.parsed)
        return filtered
        
    def _getSearchResult(self, delimeter, tokenLists):
        """
        Get the highest scored track for each tokens given in the ``tokenList`` and aggregate them in a list.
        
        :param delimeter: The delimeter which will be used to join tokens in each of the list in ``tokenList``.
        :type delimeter: str.
        :param tokenLists: list of list of tokens of the form [[token 1, token 2, ...],[...],[...], ...].
        :type tokenLists: list of list
        :return: list of dictionary with each dictionary item is of the form {'topmatch':'track name top', \
        'tracks': {'track name': {'score': x.xx, ...}, ..., 'track name top': {'score': x.xx, ...}, ...}}        
        """
        searchResult = []
        for tokens in tokenLists:
            filtered = self._getTopScoredResult(delimeter, tokens)
            if filtered:
                searchResult.append(filtered)
        return searchResult
    
    @classmethod
    def _getWeightedScore(cls, resultList):
        """
        Get a weighted score of the search result obtained from :func:`_getSearchResult`. For each
        highest scored track in sums up the score and divide by the number of tracks.
        
        :param resultList: The result that has been obtained from :func:`_getSearchResult` \
        with each dictionary item is of the form {'topmatch':'track name top', \
        'tracks': {'track name': {'score': x.xx, ...}, ..., 'track name top': {'score': x.xx, ...}, ...}}.
        :type resultList: list of dictionary.
        :return: float the weighted score of the highest scored playlist.         
        """
        if not resultList:
            return 0.0
        totalScore = 0.0
        for result in resultList:
            # make sure all the required fields are present before computing.
            if ('topmatch' in result) and ('tracks' in result) and \
            (result['topmatch'] in result['tracks']) and \
            'score' in result['tracks'][result['topmatch']]:
                totalScore += result['tracks'][result['topmatch']]['score']        
        return round(float(totalScore)/len(resultList), 2)
    
    @classmethod
    def _getFormattedResult(cls, score, resultList):
        """
        Copy the required fields from the input ``resultList`` and combine with the given ``score``
        to produce a ``dictionary`` result like {'score': score, 'tracks':[]}.
        
        :param score: The score to add in the result.
        :type score: float.
        :param resultList: List of ``dictionary``, with each dictionary like \
        {'name of track': 'href': '..', 'length': .., ...}
        :return: dictionary of the form {'score': score, 'tracks':[]}        
        """
        formatedResult = {'score': score, 'tracks':[]}
        for result in resultList:
            for track in result['tracks'].itervalues():
                formatedResult['tracks'].append({'track': track['name'], 'href': track['href'], \
                                                 'length': track['length']})
        return formatedResult
        
    def createPlayList(self, text, delimeter, queue, stopEvent):        
        """
        Creates a spotify playlist from the the given text of word(s). The given text is
        split using the ``delimeter``.

        :param text: The input text for which to create the playlist.
        :type text: str.
        :param delimeter: The delimeter to split the ``text``.
        :type delimeter: str.
        :param queue: An instance of queue on which playlist(s) will be returned to the caller.
        :type queue: Queue.
        :param stopEvent: An instance of ``threading.Event`` used as a control mechanism from the caller 
                          to terminate the method.
        :type stopEvent: Event.
        """                
        searchText = text.split(delimeter)
        maxGeneration = (len(searchText)/3) + 1
        filtered = self._getTopScoredResult(delimeter, searchText)
        if filtered and 'tracks' in filtered and text in filtered['tracks']:
            bestScore = filtered['tracks'][text]['score']
        else:
            bestScore = 0
        bestResult = self._getFormattedResult(bestScore, [filtered])
        queue.put(bestResult)
        generation = 1
        while (not stopEvent.is_set() and generation < maxGeneration):
            tokenizer = GenerationTokenizer(text, generation, ' ', 0.2)
            searchResult = self._getSearchResult(delimeter, tokenizer.tokenLists)
            score = self._getWeightedScore(searchResult)
            (bestScore, bestResult) = (bestScore, bestResult) if score < bestScore \
                        else (score, self._getFormattedResult(bestScore, searchResult))
            queue.put(bestResult)
            while tokenizer.has_more_shuffles() and (not stopEvent.is_set()):
                tokenizer.next_shuffle()
                searchResult = self._getSearchResult(delimeter, tokenizer.tokenLists)
                score = self._getWeightedScore(searchResult)
                if score > bestScore:                    
                    bestScore = score
                    bestResult = self._getFormattedResult(bestScore, searchResult)
                    queue.put(bestResult)             
            generation += 1
            if stopEvent.is_set() or generation >= maxGeneration or bestScore >= 0.99:
                return